﻿namespace SimpleX.Job
{
    /// <summary>
    /// 老规矩继承ITransient,系统自动注入
    /// </summary>
    public class ApplicationJob : BackgroundService, ITransient
    {
        private readonly IRecurringJobManager _recurringJobs;
        private readonly ILogger<ApplicationJob> _logger;
        private readonly IStudentService _studentService;

        public ApplicationJob(IRecurringJobManager recurringJobs,
            ILogger<ApplicationJob> logger,
            IStudentService studentService)
        {
            _recurringJobs = recurringJobs;
            _logger = logger;
            _studentService = studentService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                //ding
                _recurringJobs.AddOrUpdate("deleteStudent", () => _studentService.Delete(new List<BaseIdInput> { }), "* * * * *");
                _recurringJobs.AddOrUpdate("seconds", () => Console.WriteLine("Hello, seconds!"), "*/15 * * * * *");
                _recurringJobs.AddOrUpdate("minutely", () => Console.WriteLine("Hello, world!"), Cron.Minutely);
                _recurringJobs.AddOrUpdate("hourly", () => Console.WriteLine("Hello"), "25 15 * * *");
                _recurringJobs.AddOrUpdate("neverfires", () => Console.WriteLine("Can only be triggered"), "0 0 31 2 *");

                _recurringJobs.AddOrUpdate("Hawaiian", () => Console.WriteLine("Hawaiian"), "15 08 * * *", new RecurringJobOptions
                {
                    TimeZone = TimeZoneInfo.FindSystemTimeZoneById("Hawaiian Standard Time")
                });
                _recurringJobs.AddOrUpdate("UTC", () => Console.WriteLine("UTC"), "15 18 * * *");
                _recurringJobs.AddOrUpdate("Russian", () => Console.WriteLine("Russian"), "15 21 * * *", new RecurringJobOptions
                {
                    TimeZone = TimeZoneInfo.Local
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An exception occurred while creating recurring jobs.");
            }

            await Task.CompletedTask;
        }
    }
}