﻿namespace SimpleX.RBAC;

/// <summary>
/// 角色拥有的资源输出
/// </summary>
public class RoleOwnResourceOutput
{
    /// <summary>
    /// Id
    /// </summary>
    public virtual string Id { get; set; }

    /// <summary>
    /// 已授权资源信息
    /// </summary>
    public virtual List<RelationRoleMenu> GrantInfoList { get; set; }
}

public class MenuTreeSelector
{
    /// <summary>
    /// 模块id
    /// </summary>
    public string Id { get; set; }

    /// <summary>
    /// 模块名称
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// 模块图标
    /// </summary>
    public string Icon { get; set; }

    /// <summary>
    /// 模块下菜单集合
    /// </summary>
    public List<RoleGrantResourceMenu> Menu { get; set; }

    /// <summary>
    /// 授权菜单类
    /// </summary>
    public class RoleGrantResourceMenu
    {
        /// <summary>
        /// 菜单id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 父id
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 父名称
        /// </summary>
        public string ParentName { get; set; }

        /// <summary>
        /// 模块名称
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 模块id
        /// </summary>
        public string Module { get; set; }

        /// <summary>
        /// 菜单下按钮集合
        /// </summary>
        public List<RoleGrantResourceButton> Button { get; set; } = new List<RoleGrantResourceButton>();
    }

    public class RoleGrantResourceButton
    {
        /// <summary>
        /// 按钮id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        public string Apis { get; set; }
    }
}