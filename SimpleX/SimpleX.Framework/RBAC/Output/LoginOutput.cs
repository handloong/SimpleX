﻿namespace SimpleX.RBAC
{
    public class LoginOutput
    {
        /// <summary>
        /// 令牌Token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 修改密码
        /// </summary>
        public bool MustChangePwd { get; set; }
    }

    public class LoginUserOutput
    {
        /// <summary>
        /// 头像
        ///</summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 超级管理员
        ///</summary>
        public string SuperAdmin { get; set; }

        /// <summary>
        /// 手机
        ///</summary>
        public string Phone { get; set; }

        /// <summary>
        /// 邮箱
        ///</summary>
        public string Email { get; set; }

        /// <summary>
        /// 机构信息
        /// </summary>
        public string OrgName { get; set; }

        /// <summary>
        /// 机构信息全称
        /// </summary>
        public string OrgNames { get; set; }

        /// <summary>
        /// 上次登录ip
        ///</summary>
        public string LastLoginIp { get; set; }

        /// <summary>
        /// 上次登录时间
        ///</summary>
        public DateTime? LastLoginTime { get; set; }

        /// <summary>
        /// 按钮码集合
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public List<string> ButtonCodeList { get; set; }
    }

    public class PicValidCodeOutPut
    {
        /// <summary>
        /// 验证码图片，Base64
        /// </summary>
        public string ValidCodeBase64 { get; set; }

        /// <summary>
        /// 验证码请求号
        /// </summary>
        public string ValidCodeReqNo { get; set; }
    }
}