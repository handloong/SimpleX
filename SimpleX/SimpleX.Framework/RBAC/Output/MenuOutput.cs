﻿namespace SimpleX.RBAC
{
    public class MenuOutput
    {
        //public string RouteName { get; set; }

        public string MethodName { get; set; }

        public string MethodPath { get; set; }

        public string MethodCode { get; set; }

        public ActionType AuthType { get; set; }
    }
}