﻿namespace SimpleX.RBAC;

/// <summary>
/// 模块分页输入
/// </summary>
public class ModulePageInput : BasePageInput
{
}

/// <summary>
/// 添加模块输入
/// </summary>
public class ModuleAddInput : SysMenu
{
}

/// <summary>
/// 编辑模块输入
/// </summary>
public class ModuleEditInput : ModuleAddInput
{
    /// <summary>
    /// ID
    /// </summary>
    [Required(ErrorMessage = "Id不能为空")]
    public override string Id { get; set; }
}