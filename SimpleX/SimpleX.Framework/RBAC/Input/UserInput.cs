﻿namespace SimpleX.RBAC
{
    /// <summary>
    /// 用户分页查询参数
    /// </summary>
    public class UserPageInput : BasePageInput
    {
        /// <summary>
        /// 所属组织
        /// </summary>
        public string OrgId { get; set; }

        /// <summary>
        /// 动态查询条件
        /// </summary>
        public Expressionable<SysUser> Expression { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>

        public string UserStatus { get; set; }
    }

    /// <summary>
    /// 添加用户参数
    /// </summary>
    public class UserAddInput : SysUser
    {
        /// <summary>
        /// 账号
        /// </summary>
        [Required(ErrorMessage = "Account不能为空")]
        public override string Account { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [Required(ErrorMessage = "Name不能为空")]
        public override string Name { get; set; }
    }

    /// <summary>
    /// 用户授权角色参数
    /// </summary>
    public class UserGrantRoleInput
    {
        /// <summary>
        /// Id
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public string Id { get; set; }

        /// <summary>
        /// 授权权限信息
        /// </summary>
        [Required(ErrorMessage = "RoleIdList不能为空")]
        public List<string> RoleIdList { get; set; }
    }

    /// <summary>
    /// 编辑用户参数
    /// </summary>
    public class UserEditInput : UserAddInput
    {
        /// <summary>
        /// Id
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public override string Id { get; set; }
    }

    /// <summary>
    /// 更新个人密码
    /// </summary>
    public class UpdatePasswordInput
    {
        /// <summary>
        /// 工号
        /// </summary>
        [Required(ErrorMessage = "Account不能为空")]
        public string Account { get; set; }

        /// <summary>
        /// 老密码
        /// </summary>
        [Required(ErrorMessage = "Password不能为空")]
        public string Password { get; set; }

        /// <summary>
        /// 新密码
        /// </summary>
        [Required(ErrorMessage = "NewPassword不能为空")]
        public string NewPassword { get; set; }
    }

    /// <summary>
    /// 编辑个人信息参数
    /// </summary>
    public class UpdateInfoInput : SysUser
    {
        ///// <summary>
        ///// 姓名
        ///// </summary>
        //[Required(ErrorMessage = "Name不能为空")]
        //public override string Name { get; set; }
    }

    /// <summary>
    /// 更新个人工作台
    /// </summary>
    public class UpdateWorkbenchInput
    {    /// <summary>
         /// 工作台数据
         /// </summary>
        [Required(ErrorMessage = "WorkbenchData不能为空")]
        public string WorkbenchData { get; set; }
    }
}