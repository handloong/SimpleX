﻿namespace SimpleX.RBAC;

public class ButtonPageInput : BasePageInput
{
    /// <summary>
    /// 父ID
    /// </summary>
    [Required(ErrorMessage = "ParentId不能为空")]
    public string ParentId { get; set; }
}

/// <summary>
/// 添加按钮参数
/// </summary>
public class ButtonAddInput : SysMenu
{
    /// <summary>
    /// 父ID
    /// </summary>
    [Required(ErrorMessage = "ParentId不能为空")]
    public override string ParentId { get; set; }

    /// <summary>
    /// 标题
    /// </summary>
    [Required(ErrorMessage = "Title不能为空")]
    public override string Title { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [Required(ErrorMessage = "Code不能为空")]
    public override string Code { get; set; }
}

public class ButtonEditInput : ButtonAddInput
{
    /// <summary>
    /// ID
    /// </summary>
    [Required(ErrorMessage = "Id不能为空")]
    public override string Id { get; set; }
}