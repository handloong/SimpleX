﻿namespace SimpleX.RBAC;

/// <summary>
/// 角色添加参数
/// </summary>
public class RoleAddInput : SysRole
{
    /// <summary>
    /// 名称
    /// </summary>
    [Required(ErrorMessage = "Name不能为空")]
    public override string Name { get; set; }

    /// <summary>
    /// 分类
    /// </summary>
    [Required(ErrorMessage = "分类不能为空")]
    public override string Category { get; set; }
}

/// <summary>
/// 角色编辑参数
/// </summary>
public class RoleEditInput : RoleAddInput
{
    /// <summary>
    /// Id
    /// </summary>
    [Required(ErrorMessage = "Id不能为空")]
    public override string Id { get; set; }
}

public class RolePageInput : BasePageInput
{
}

public class GrantMenuInput
{
    /// <summary>
    /// 角色Id
    /// </summary>
    [Required(ErrorMessage = "Id不能为空")]
    public string Id { get; set; }

    /// <summary>
    /// 授权资源信息
    /// </summary>
    [Required(ErrorMessage = "GrantInfoList不能为空")]
    public List<RelationRoleMenu> GrantInfoList { get; set; }
}

public class RelationRoleMenu
{
    /// <summary>
    /// 菜单ID
    /// </summary>
    public string MenuId { get; set; }

    /// <summary>
    /// 按钮信息
    /// </summary>
    public List<string> ButtonInfo { get; set; } = new List<string>();
}