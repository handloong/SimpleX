﻿namespace SimpleX.RBAC;

/// <summary>
/// 组织分页查询参数
/// </summary>
public class SysOrgPageInput : BasePageInput
{
    /// <summary>
    /// 父ID
    /// </summary>
    public string ParentId { get; set; }

    /// <summary>
    /// 机构列表
    /// </summary>
    public List<string> OrgIds { get; set; }
}

/// <summary>
/// 组织添加参数
/// </summary>
public class SysOrgAddInput : SysOrg
{
}

/// <summary>
/// 组织修改参数
/// </summary>
public class SysOrgEditInput : SysOrgAddInput
{
    /// <summary>
    /// Id
    /// </summary>
    public override string Id { get; set; }
}