﻿namespace SimpleX.RBAC
{
    public class LoginDevices
    {
        public static List<string> AllowDevices => new List<string> { "web", "pda", "android" };
    }

    public class LoginInput
    {
        public string Account { get; set; }
        public string Password { get; set; }
        public string Device { get; set; }
    }

    public class LoginInputValidator : AbstractValidator<LoginInput>, IScoped
    {
        public LoginInputValidator()
        {
            RuleFor(x => x.Account)
                .NotNull()
                .MinimumLength(3)
                .MaximumLength(20)
                .WithMessage("八嘎,用户名必须在3-20位");

            RuleFor(x => x.Password)
                .NotNull()
                .MinimumLength(2);

            RuleFor(x => x.Device).NotNull()
              .Must(v => LoginDevices.AllowDevices.Contains(v))
              .WithMessage("当前设备不支持登陆本系统");

            //RuleFor(x => x.Email).NotNull().EmailAddress()
            //  .Must(v => v.EndsWith("@qq.com") || v.EndsWith("@163.com"))
            //  .WithMessage("只支持QQ和163邮箱");
            //RuleFor(x => x.Password).NotNull().Length(3, 10)
            //    .WithMessage("密码长度必须介于3到10之间")
            //    .Equal(x => x.Password2).WithMessage("两次密码必须一致");
        }
    }

    public class LoginOutInput
    {
        public string Token { get; set; }
        public string Device { get; set; } = "web";
    }
}