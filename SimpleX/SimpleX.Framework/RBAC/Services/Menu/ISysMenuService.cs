﻿namespace SimpleX.RBAC
{
    public interface ISysMenuService : ITransient
    {
        Task<List<SysMenu>> GetOwnMenus();

        Task Add(MenuAddInput input);

        List<SysMenu> ConstructMenuTrees(List<SysMenu> resourceList, string parentId = SimpleXConst.Zero);

        Task<List<SysMenu>> GetListByCategory(string category);

        Task<List<SysMenu>> Tree(MenuTreeInput input);

        Task Edit(MenuEditInput input);

        Task Delete(List<BaseIdInput> input);

        Task ChangeModule(MenuChangeModuleInput input);

        List<MenuOutput> GetAuthCodeAttributes(MenuAuthInput input);

        Task<List<MenuTreeSelector>> ResourceTreeSelector();
    }
}