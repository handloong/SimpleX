﻿namespace SimpleX.RBAC
{
    public interface ISysOrgService : ITransient
    {
        Task<List<SysOrg>> Tree(List<string> orgIds = null);

        Task<SqlSugarPagedList<SysOrg>> Page(SysOrgPageInput input);

        Task<List<string>> GetOrgChildIds(string orgId, bool isContainOneself = true);

        Task<List<SysOrg>> GetChildListById(string orgId, bool isContainOneself = true);

        Task Add(SysOrgAddInput input, string name = SimpleXConst.SysOrg);

        Task Edit(SysOrgEditInput input, string name = SimpleXConst.SysOrg);

        Task Delete(List<BaseIdInput> input, string name = SimpleXConst.SysOrg);

        List<SysOrg> ConstrucOrgTrees(List<SysOrg> orgList, string parentId = SimpleXConst.Zero);
    }
}