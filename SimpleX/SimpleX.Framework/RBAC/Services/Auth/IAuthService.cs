﻿namespace SimpleX.RBAC
{
    public interface IAuthService : ITransient, ICapSubscribe
    {
        Task<LoginOutput> Login(LoginInput loginInput);

        PicValidCodeOutPut GetCaptchaInfo();

        bool CheckTokenInRedis(string token);

        Task<LoginUserOutput> GetLoginUser();

        Task LoginOut(LoginOutInput input);
    }
}