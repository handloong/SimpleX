﻿namespace SimpleX.RBAC
{
    public interface IRelationService : ITransient
    {
        Task<List<SysRelation>> GetRelationByCategory(string category);

        Task SaveRelationBatch(string category, string objectId, List<string> targetIds, List<string> extJsons, bool clear);

        Task SaveRelation(string category, string objectId, string targetId, string extJson, bool clear);

        Task<List<SysRelation>> GetRelationListByObjectIdAndCategory(string objectId, string category);
    }
}