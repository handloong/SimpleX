﻿namespace SimpleX.RBAC
{
    public interface IRoleService : ITransient
    {
        Task<SqlSugarPagedList<SysRole>> Page(RolePageInput input);

        Task<List<SysRole>> RoleSelector(string searchKey);

        Task Add(RoleAddInput input);

        Task Delete(List<BaseIdInput> input);

        Task Edit(RoleEditInput input);

        Task<List<SysRole>> GetRoleListByUserId(string userId);

        /// <summary>
        /// 给角色授权
        /// </summary>
        /// <param name="input">授权参数</param>
        /// <returns></returns>
        Task GrantMenu(GrantMenuInput input);

        Task<RoleOwnResourceOutput> OwnResource(BaseIdInput input);
    }
}