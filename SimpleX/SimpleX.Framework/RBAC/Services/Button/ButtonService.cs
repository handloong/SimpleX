﻿namespace SimpleX.RBAC;

/// <summary>
/// <inheritdoc cref="IButtonService"/>
/// </summary>
public class ButtonService : DbRepository<SysMenu>, IButtonService
{
    /// <inheritdoc/>
    public async Task<SqlSugarPagedList<SysMenu>> Page(ButtonPageInput input)
    {
        var query = Context.Queryable<SysMenu>()
                         .Where(it => it.ParentId == input.ParentId && it.Category == CateGoryConst.Menu_BUTTON)
                         .WhereIF(!string.IsNullOrEmpty(input.SearchKey), it => it.Title.Contains(input.SearchKey) || it.Path.Contains(input.SearchKey))//根据关键字查询
                         .OrderByIF(!string.IsNullOrEmpty(input.SortField), $"{input.SortField} {input.SortOrder}")
                         .OrderBy(it => it.SortCode);//排序
        var pageInfo = await query.ToPagedListAsync(input.Current, input.Size);//分页
        return pageInfo;
    }

    /// <inheritdoc />
    public async Task Add(ButtonAddInput input)
    {
        var result = await CheckInput(input, true);//检查参数
        if (result != "OK")
        {
            Unify.SetError(result);
            return;
        }
        var sysResource = input.Adapt<SysMenu>();//实体转换
        await InsertAsync(sysResource);
    }

    /// <inheritdoc />
    public async Task Edit(ButtonEditInput input)
    {
        var result = await CheckInput(input, false);//检查参数
        if (result != "OK")
        {
            Unify.SetError(result);
            return;
        }
        var sysResource = input.Adapt<SysMenu>();//实体转换
        //事务
        var tran = await itenant.UseTranAsync(async () =>
        {
            await UpdateAsync(sysResource); //更新按钮
        });
        if (!tran.IsSuccess)//如果成功了
        {
            throw tran.ErrorException;
        }
    }

    /// <inheritdoc />
    public async Task Delete(List<BaseIdInput> input)
    {
        //获取所有ID
        var ids = input.Select(it => it.Id).ToList();
        if (ids.Count > 0)
        {
            var menus = await GetListAsync(x => ids.Contains(x.Id));

            var result = await itenant.UseTranAsync(async () =>
            {
                await DeleteByIdsAsync(ids.Cast<object>().ToArray());
                await Context.Deleteable<SysRelation>().Where(x => ids.Contains(x.ObjectId) || ids.Contains(x.TargetId)).ExecuteCommandAsync();
            });
            if (!result.IsSuccess)//如果成功了
            {
                throw result.ErrorException;
            }
        }
    }

    #region 方法

    /// <summary>
    /// 检查输入参数
    /// </summary>
    /// <param name="sysResource"></param>
    /// <param name="add"></param>
    /// <returns></returns>
    private async Task<string> CheckInput(SysMenu sysResource, bool add)
    {
        if (add)
        {
            var any = await IsAnyAsync(x => x.Category == CateGoryConst.Menu_BUTTON && x.Code == sysResource.Code);
            if (any)
                return ($"存在重复的按钮编码:{sysResource.Code}");
        }
        else
        {
            var any = await IsAnyAsync(x => x.Category == CateGoryConst.Menu_BUTTON && x.Code == sysResource.Code && x.Id != sysResource.Id);
            if (any)
                return ($"存在重复的按钮编码:{sysResource.Code}");
        }

        sysResource.Category = CateGoryConst.Menu_BUTTON;

        return "OK";
    }

    #endregion 方法
}