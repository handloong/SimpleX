﻿using Masuit.Tools;
using SimpleX.Sys;

namespace SimpleX.RBAC
{
    public class SysUserService : DbRepository<SysUser>, ISysUserService
    {
        private readonly ISysOrgService _sysOrgService;
        private readonly ICacheService _cacheService;
        private readonly IRelationService _relationService;
        private readonly IConfigService _configService;

        public SysUserService(ISysOrgService sysOrgService,
            ICacheService cacheService,
            IRelationService relationService,
            IConfigService configService)
        {
            _sysOrgService = sysOrgService;
            _cacheService = cacheService;
            _relationService = relationService;
            _configService = configService;
        }

        public async Task Add(UserAddInput input)
        {
            var check = await CheckInput(input);//检查参数
            if (check != "OK")
            {
                Unify.SetError(check);
                return;
            }
            var sysUser = input.Adapt<SysUser>();//实体转换
            if (string.IsNullOrWhiteSpace(sysUser.Password))
            {
                sysUser.Password = PwdUtils.DefaultPwd();//设置密码
            }
            sysUser.UserStatus = "Y";//默认状态
            sysUser.Avatar = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAAD55JREFUeF7tXdth27gSHSo9bPIXpZIoJaiCtT/iTRe2u8h1PpxUoBLiVLLcPzs9mLyBScoUBRKvGRIgjv92AwzAM3M0D7wKwh8QAAKjCBTABggAgXEEQJAIrOPd4ctWN43H/dcygullPQUQREj9rdFvK6o+qiEKqnevQxWKEFpSjEynR5S6rGlTEtXlhur/iN6ofytBJhlFgiCBuDZEeN5VVLxvSOBs/IEzOHYvC9p8AlG44GzkgCCOeCpCVFT93ZKh5xUcBQk0f9rfQZ/MuAJQA6AxE6I/9ZqK77/3/7tkto/sxYEgGhN4JQXdpGIhBdWXj/tv31OZbyrzBEFaTaVIir6RFbT5gPyDn3ZZE6RHigvHqhK/JgIlIv8IBHCke5YEaYhRXxdUK2Ik/4f8Q06FWRHk3eHLrqbna6IiqupTqHqRf4QiON4/C4I0xKjuUw+jxtSI/AME8ULgr8PVdUGUfH5h+njkHyaE/P99lR4kF2K0ai+f9ncf/E0APacQWBVBVPJd0/P92nKMaROuH5723z7BzGUQWAVBUl/DCFEtKlgh6Jn7Jk+Qd4fPFzUVKgHP8g8EkVV7sgTJM5w6N4aa6Ob3/u5W1kzylZ4kQdokPJl9UpLmhTUQSXQT2+4Or3FuDCAICPKCQLvY91MWjvSkt4ekHtKbeRozTiLEQkg1bkwgiCzRoiYIQiqz8kEQM0YhLaIlCEIqO7WCIHY4+baKkiC5r224KBNJugta7m2jI8hfh3/u13JOw10d7j1AEHfMXHpERZC3h88/89pH5aIqfVssFIZjOCUhGoKAHH6KBkH8cLPttThBUKmyVdVYO+zmDUUwWg8CcrCoFudBWGDUC1nMgzTkqP4V/LZsRONEoZyqFyEIPAevQrEWwotnX9oiBEFCzqtQEIQXz0UJAnLwKxOVLH5MO4mzehCQQ0yRSNSFoJ2NIFghF9JgKxaJugy+sxAE29VllHcaK788noNzIcxQixMEu3KZNTYiDpc3yOAsShCsdcgobUQq8hABuEUJgqRcQGMTIlHu5cdbjCAgB7+yTBIRZpkQcv93EYIg73BXBFMPhFlMQIqtgyDvYNaQoziEWY6AGZqzexCEVrwKcpeG7e/umI33YCUIyMGpGm9ZZetFSm8J6HhEgI0gyDvisSok63y6YCMIvAefUhgkwYswgKhEsBAEW0mYtMEoBl6EB8xggqBqxaMIASko+TKAGkwQhFYMWhASAS8SDmwQQZCYhytgIEFVnraMUlUucoldvv6IBhHk7eFKXbrAqVD/L0m2Z/1QU/Gwoc2Px/3XUoWszac872oqrsPxxbpIiGl4EwSJeQjsTV/TUdnmcdL6OvQqVlxP6q8rb4K8PVz90S/+fBGw3RLC9IIvEnZPRXkRBN7DE+22my05+qOEYo6E3U9nXgSB9/ADW/XyDXc47hIzhXT+X7Xens4Ewdsd/sYQ+iverjmpdxp9CyOoajmqz5kgqFw5InxszlNN4iDJ0/7ug+9X5NbPiSDwHr7mwUOObvTQ3QuhnswXhRT7OREE3sNLxSIVpNAfK+Qjdrq0JkioQuyms75WPhUrWxRCdQKSmJG2Jgi8hxnMYQtJcnRjhZd/6eb3/u7W/evy6GFFEOy5cjeGOcjBRJKyoPr2cf/tu/tXrr+HFUFwr66bIcwdujCskZQ10Xd4knM9WxEE4ZULQXgrVrYjc5R/QRIPgoQmgrYKXke7ZcgxKP+GLCQaN1CuQ0/2X2H0IAivrMEUKedaj942DF0jUWLmDhFdv3HO9kaCYN+VlTqiIMerJ/l8UVNxbzXz8Ua4+MF0aQPCKzsTm7NiZTcjotDybztO9sn7pAfBeXOzOcZIDqby7/Hjcw65DATBoagpisRMDjVvhvJv//Oz3Ak8ShCEV9PeI5VfVWaSkNrouKHiVp2fN/vX9FuMEgTVqynlLlvOdTU7hjWS4ZDZ5CajBMHi4JgZpkWOwRqJuoWG82/1RJkiCC5lODelqMq5rpYuuKdutUTREgT5h9b0VrEuIKzbF6J0d3y5Enip9u1dZFvdBXtagiD/OFdV7BUrF+NiWiMxDFk/FEQ/Yt0l3LtO6aI5468PnbUEQf5xqvs1kYN7jcSCmGVzc2T1a2myvJKi3hEVu8HcteHzGUEE41QLLONr4ntNT3xfcjoj7vKvw/fORpiOEGpufwz9xjBHW4Kw7ONxwCvepqmsdfgiKFD+9ZlKSVSXNW1K5WUaAW/UGsvLOsvUesvrPcaq5fOuouI9UbEtqNpqPIRxbrpI4cyDIP/ocEyznGu0gkGDSEhiM+1uYdL3TjDjGLpo4YwgyD8UjnmQo7MYji3yRutLoIEuYgBBzhWX9FqHrx0Kl399pzVzv/MfRh1Bcl4gzHJDXmeF85R/Z7Z5t+HOfhxPCJJ7BWuN5Vw3+2A7R+I6bDTthzYwIEi+FSyQo7FRrkd7orF4x4lMEiRXF7v2cq6jjXCfI3EdftH2Q1s48SB5lnjzqljZWl++la1TezghSI4lXoRW45TJ88j1NEGyq2A97e+MN7vY/uqurV2mRZuTStbQg2RFELyTMU3pTMMsEKQzi7VuROT0ZBmGWXqC5PhrAYKYqZQhQagfdh9DLBDEbCw5tsi0cPOh20XcJ8iupkpdfJzNH3IQs6pzvHq2oA0I0ppGlhsTzbToVtXz3FnRL/33PEieYGAVHesgQwRAkFNE8ASZhiM5Jue66mb2HqQLtfC60utmxZqe732OrNqGbrG361c3jwTJdaOiRlkvZ6S5lPi0//aJS5ZOTrN/Tp3B5vg7u+mDQ2hyMvphNzyIrPrEiwA5lmFlVaZuQKkvuyuKQBBZtEEQWXxFpI8Q5Et26yAi6A4KAE/7uw+S48CD8KMLgvBjOiYRHmQ+rNlGGinzwoOwIfwqCAQRAFVa5NhK+ramivv9COlviV0+CBK7hjTzA0HmUxoIMh/WbCOBIGxQGgWBIEaI4mug3e6uppnjzk1h9YAgwgBLiJ8iiMpBmFZlJaaenEwQJDmVkf7AVONBPv/MeQ+OgC5BEAFQZUVO3GqS571YonCDIKLw8gsfHqIbXBx3dW3xEg//rNYrEQRJTLfDewpwebWsAkEQWXzZpZsIgsVCXshBEF48xaX110DUYEMPAoLwqgAE4cVTXNrwpk3NAzqoZDFqAQRhBHMGUdMP6KDUy64CEIQdUjmBumugNK/copLFqAIQhBFMaVFWBMn0Rm8p7EEQKWQF5Fo9A53jFaQCWHciQRBBcLlFDytYZ1WsbkBsOWGDHgRhg1JakP6lMe3jMbgCiE0ZIAgblLKCxu5p1hIEeQibMkAQNihlBY09hTFGECwY8ugDBOHBUVzK2FN8o+/zIQ9h0QkIwgKjrJCpZzBGCYI8hEUpIAgLjLJCvAiCPIRFKSAIC4yyQqae4hv1IFgPYVEKCMICo6yQqafAJ98IxwnDYMWAIMEQygowPcM3SRCEWcHKmYMgWb1tH6yRgYD+NaM62ZMEUR1QzQpTiUkBIdLfHfJ8Ni8Es2HfqfBKtTUSBNWsMHWYXHiIdPx4haBHZKMbI0GQrIcpgYjKgjaXj/uvD8GSegIQ/oajqducOJRqJAjCrHBFKJJwvoEIr86hE/3mRC+CINblUEjj0jdU/SJ6o95AdH0HcVtR9bEgusDtl+H6mFr76Eu38iAIs8IVAglxIWATXlkl6d1nYU0kLgVjNv4I2CTnnXQrD6Iaw4v4KwQ940LApfRuTRAk63EpGbPxQ8DFeziFWPAifgpBr7gQcPEezgSBF4lL2ZiNGwKu3sOLIFigclMKWseDgKv38CIIvEg8CsdMXBCwWxgcSnRK0rvO8CIuikHbGBDw8R7eHgReJAaVYw72CPh5jyCCYF3EXj1ouSwCvt4jiCCqM1bXl1U8Rjcj4FO56kv1ykF6uYi6P+snNs+ZFaVa/Dn6d7OhzS+fre/KYyuc2w2LN3YjopXpQJQJoSCCKOHY6WuC+OXfWc+EtOEtfpgM0Nvu2J0SE0wQJOxmgoTEwGPSkQOacPdPzNlCrEGo9a9pyjn+uwqrfu/vbiW+Hd57HFWuHyUWD9Ik7HiZSqcu23MHPgSCF9GjFpqYs3uQJhf5goRdo6/QJNFEnLeHK+W5VQKPvwYB1quW2DxIS5JdW9WCsgSUpQMVBDlFhSu06qSyEgSh1rkJcytsOMLbwxUujmtBkcj32AnShFrP90TFDm7E7u4lX5yQpPeR46laDXXBThDkI2fmzroG0klHgn6CM2veIZKkD80CO35PFdiGWq5X/WidC7y0bN4xC0GQj5x7EnV5nO9Wk84zV1T9/cftY6uJYN4xG0HwSzeaXfh4EpRyz+CUyTtmIwjyEd/0G/0sEBDLO2YlCEhioWo0cUVApPChm4RIFUs3EKourjaA9mMISK8tze5BUJqEsXMhMCc51Jxn8yCvJMGrSFzGkpucucmxCEFQ/s3NrHm+l+Pwk89MZvcg3SSxPd5HXXn2kdhjZYvkYgSBJ7FVUd7tOM92+CC5KEFAEh+V5dNnqbBqsSrWmGoRbuVj9LZfukRCrpvb4h6kVwLGYStb61l5u1jIsVgVa0y/OLa7css3f95sK+TmqTQtovEgPU+Cs+222ltXu+jIESVB1KTgSdZl+RZfM8vGQ4t5nDWJzoP0PQnOPvioNK0+S65x2CAVLUGQvNuoL+k2UYZUQ0SjJwhCrqRJMDZ5RY5Pj/uvPgfHZgUkCYJ0JEHINattiAwWe0iVpAfpTxqXQYjY7RxCkwipkicIvMkctsw9hvzZce4Zd/KSCbF0ALTe5B5300qZR7DcJL1G/6uTJgi8SbABiwlILdcYAyJ5gvTKwduK6uuC6gsxrUOwBQL1Q0FvLlOoUFl8THxbTWwmPdWmva/2GmFXKJLO/ZMPp3RfvBoPMqh0qccu1Q2EypvgwjVnW3fqUKobI6Ve0XKaiUDjVRKkH3YRPe9qKuBR+I1n1cRYRRXLVufNE8qKKPQ3nmWwRW20XRbEyIogp+GXunYIRPGgSVlQffu4//bdo2+yXVYdYk0n81+6PAU3pY8D9eItNrT5sZaqlCtTsyXIME+paPMRJeIOFVWqpR+5eYtsqliuvxLnSX2WIVhWuYWtjWTvQcaAyiCxL4nqsqbiIecQykQUEMSEUHsEWK2nVFSpMGyXcCWsyyl+Pe6/Plh8evZNQBAPE2i8S/SEeTmMFPrsmwc8q+oCgjCpsyMN0fNWJfxKbEHVdgZvcwyV1JghbyAyQbEqMSDIDOp8JY8a7Pm49aWi4j1RcfzvhlDqV3/THkWtj0dSN1T/10z1Tff/ylxLrzOo7DgECDIn2hgrOQRAkORUhgnPicD/AYUK9DIK8uUVAAAAAElFTkSuQmCC";

            await InsertAsync(sysUser);//添加数据
        }

        public async Task Delete(List<BaseIdInput> input)
        {
            var ids = input.Select(it => it.Id).ToList();
            if (ids.Count > 0)
            {
                foreach (var item in input)
                {
                    DeleteUserCache(item.Id);
                }

                var result = await itenant.UseTranAsync(async () =>
                {
                    //删除用户
                    await DeleteByIdsAsync(ids.Cast<object>().ToArray());
                    var relationRep = ChangeRepository<DbRepository<SysRelation>>();//切换仓储
                    //删除关系表
                    var delRelations = new List<string> { CateGoryConst.Relation_SYS_USER_HAS_MENU, CateGoryConst.Relation_SYS_USER_HAS_ROLE };
                    await relationRep.DeleteAsync(it => ids.Contains(it.ObjectId) && delRelations.Contains(it.Category));
                });
                if (result.IsSuccess)//如果成功了
                {
                }
                else
                {
                    throw result.ErrorException;
                }
            }
        }

        public async Task Edit(UserEditInput input)
        {
            await CheckInput(input);//检查参数
            var exist = await GetByIdAsync(input.Id);//获取用户信息
            if (exist != null)
            {
                var isSuperAdmin = exist.Account == "SuperAdmin";//判断是否有超管
                if (isSuperAdmin && !UserManager.SuperAdmin)
                {
                    Unify.SetError($"不可修改系统内置超管用户账号");
                    return;
                }

                var name = exist.Name;//姓名
                var sysUser = input.Adapt<SysUser>();//实体转换
                if (await Context.Updateable(sysUser).IgnoreColumns(it =>
                        new
                        {
                            //忽略更新字段
                            it.Password,
                            it.LastLoginIp,
                            it.LastLoginTime,
                        }).ExecuteCommandAsync() > 0)//修改数据
                {
                    //删除用户token缓存
                    DeleteUserCache(input.Id);
                }
            }
        }

        public async Task<SysUser> GetSysUserByAccount(string account)
        {
            var user = await GetFirstAsync(x => x.Account == account);
            //针对不忽略大小写的数据库再次进行强制验证大小写是否一致
            if (user?.Account != account)
            {
                return default;
            }
            return user;
        }

        public async Task<SysUser> GetSysUserById(string userId)
        {
            var user = await Context.Queryable<SysUser>()
                            .LeftJoin<SysOrg>((u, o) => u.OrgId == o.Id)
                            .Where(u => u.Id == userId)
                            .Select((u, o) => new SysUser
                            {
                                Id = u.Id.SelectAll(),
                                OrgName = o.Name,
                                OrgNames = o.Names
                            }).FirstAsync();
            return user;
        }

        public async Task SetLogined(string userId)
        {
            await Context.Updateable<SysUser>()
                .SetColumns(x => x.LastLoginTime == DateTime.Now)
                .Where(x => x.Id == userId)
                .ExecuteCommandAsync();
        }

        public async Task GrantRole(UserGrantRoleInput input)
        {
            var sysUser = await GetByIdAsync(input.Id);//获取用户信息
            if (sysUser != null)
            {
                if (sysUser.SuperAdmin == "Y")
                {
                    Unify.SetError($"不能给超管分配角色");
                    return;
                }
                //删除用户的接口缓存信息
                DeleteUserCache(input.Id);

                await _relationService.SaveRelationBatch(CateGoryConst.Relation_SYS_USER_HAS_ROLE, input.Id, input.RoleIdList.Select(it => it.ToString()).ToList(), null, true);
            }
        }

        public async Task<SqlSugarPagedList<SysUser>> Page(UserPageInput input)
        {
            var query = await GetQuery(input);//获取查询条件
            var pageInfo = await query.ToPagedListAsync(input.Current, input.Size);//分页
            return pageInfo;
        }

        public async Task<List<string>> OwnRole(BaseIdInput input)
        {
            var relations = await _relationService.GetRelationListByObjectIdAndCategory(input.Id, CateGoryConst.Relation_SYS_USER_HAS_ROLE);
            return relations.Select(it => it.TargetId).ToList();
        }

        public async Task<List<string>> OwnButtonCodeList()
        {
            if (UserManager.SuperAdmin)
            {
                var codes = await Context.Queryable<SysMenu>().Where(x => x.Category == CateGoryConst.Menu_BUTTON)
                                .Select(x => x.Code)
                                .ToListAsync();

                return codes;
            }
            else
            {
                var roleIds = await OwnRole(new BaseIdInput { Id = UserManager.UserId });

                //角色包含多少菜单
                var relations = await Context.Queryable<SysRelation>()
                                .InnerJoin<SysMenu>((x, y) => x.TargetId == y.Id)
                                .Where((x, y) => roleIds.Contains(x.ObjectId) && x.Category == CateGoryConst.Relation_SYS_ROLE_HAS_MENU)
                                .Select((x, y) => x)
                                .ToListAsync();

                //按钮Id
                var buttonIds = relations.SelectMany(x => x.ExtJson.ToObject<List<string>>()).Distinct();

                var codes = await Context.Queryable<SysMenu>().Where(x => x.Category == CateGoryConst.Menu_BUTTON && buttonIds.Contains(x.Id))
                                .Select(x => x.Code)
                                .ToListAsync();
                return codes;
            }
        }

        public async Task<List<string>> OwnPermissionCodeList()
        {
            //用户的包含的APIs
            var result = new List<string>();
            result = _cacheService.HashGetOne<List<string>>(CacheConst.Cache_UserRelation, CacheConst.Field_UserHasApi(UserManager.UserId));
            if (result != null && result.Count > 0)
            {
                return result;
            }
            result ??= [];

            var roleIds = await OwnRole(new BaseIdInput { Id = UserManager.UserId });

            //角色包含多少菜单
            var relations = await Context.Queryable<SysRelation>()
                            .InnerJoin<SysMenu>((x, y) => x.TargetId == y.Id)
                            .Where((x, y) => roleIds.Contains(x.ObjectId) && x.Category == CateGoryConst.Relation_SYS_ROLE_HAS_MENU)
                            .Select((x, y) => x)
                            .ToListAsync();

            //菜单Id + 按钮Id
            var menuIds = relations.Select(x => x.TargetId).ToList();
            var buttonIds = relations.SelectMany(x => x.ExtJson.ToObject<List<string>>());
            menuIds.AddRange(buttonIds);
            menuIds = menuIds.Distinct().ToList();

            //菜单对应的菜单和按钮
            var menus = await Context.Queryable<SysMenu>()
                             .Where(x => menuIds.Contains(x.Id))
                             .ToListAsync();

            foreach (var item in menus)
            {
                if (!string.IsNullOrEmpty(item.Apis))
                {
                    result.AddRange(item.Apis.Split(','));
                }
            }

            if (result.Count > 0)
                _cacheService.HashAdd(CacheConst.Cache_UserRelation, CacheConst.Field_UserHasApi(UserManager.UserId), result);

            return result;
        }

        public async Task DisableUser(BaseIdInput input)
        {
            var sysUser = await GetByIdAsync(input.Id);//获取用户信息
            if (sysUser != null)
            {
                var innerAdmin = sysUser.Account == "SuperAdmin";//判断是否有超管
                if (innerAdmin)
                {
                    Unify.SetError($"不可禁用系统内置超管用户账号");
                    return;
                }

                //删除用户的接口缓存信息
                DeleteUserCache(input.Id);

                await UpdateAsync(it => new SysUser
                {
                    UserStatus = "N",
                }, it => it.Id == input.Id);
            }
        }

        public async Task EnableUser(BaseIdInput input)
        {
            await UpdateAsync(it => new SysUser
            {
                UserStatus = "Y"
            }, it => it.Id == input.Id);
        }

        public async Task ResetPassword(BaseIdInput input)
        {
            DeleteUserCache(input.Id);
            var password = PwdUtils.DefaultPwd();
            var lastLoginTime = new DateTime(1996, 5, 21);
            await UpdateAsync(it => new SysUser
            {
                LastLoginTime = lastLoginTime,
                Password = password,
            }, it => it.Id == input.Id);
        }

        public async Task<dynamic> UpdatePassword(UpdatePasswordInput input)
        {
            //获取用户信息
            var oldPassword = PwdUtils.Decrypt(input.Password);
            var userInfo = await GetFirstAsync(x => x.Account == input.Account);
            if (userInfo == null)
                return Unify.SetError("原密码错误");

            var dbPassword = PwdUtils.Decrypt(userInfo.Password);
            if (dbPassword != oldPassword)
                return Unify.SetError("原密码错误");

            //通常来说，如果相似度高于80%左右，那么我们可以认为这两个密码相对较相似。
            var newPassword = PwdUtils.Decrypt(input.NewPassword);
            var similarity = PwdUtils.Similarity(dbPassword, newPassword);
            if (similarity > 80)
                return Unify.SetError($"新密码请勿与旧密码过于相似");

            if (!PwdUtils.IsValidPassword(newPassword))
                return Unify.SetError("新密码不符合规则，请包含至少一个大写字母、一个小写字母、一个数字和一个特殊字符，并且长度至少6个字符");

            userInfo.Password = input.NewPassword;
            userInfo.LastLoginTime = DateTime.Now;
            await Context.Updateable(userInfo).UpdateColumns(it => new { it.Password, it.LastLoginTime }).ExecuteCommandAsync();//修改密码

            return default;
        }

        public async Task<string> UpdateAvatar(BaseFileInput input)
        {
            var userInfo = await GetByIdAsync(UserManager.UserId);
            var file = input.File;
            using var fileStream = file.OpenReadStream();//获取文件流
            byte[] bytes = new byte[fileStream.Length];
            fileStream.Read(bytes, 0, bytes.Length);
            fileStream.Close();
            var base64String = Convert.ToBase64String(bytes);//转base64
            var avatar = "data:image/png;base64," + base64String;//转图片
            userInfo.Avatar = avatar;
            await Context.Updateable(userInfo).UpdateColumns(it => new { it.Avatar }).ExecuteCommandAsync();//修改密码
            return avatar;
        }

        public async Task<dynamic> UpdateUserInfo(UpdateInfoInput input)
        {
            //如果手机号不是空
            if (!string.IsNullOrEmpty(input.Phone))
            {
                if (!input.Phone.MatchPhoneNumber())//判断是否是手机号格式
                    return Unify.SetError($"手机号码格式错误");
                var any = await IsAnyAsync(it => it.Phone == input.Phone && it.Id != UserManager.UserId);//判断是否有重复的
                if (any)
                    return Unify.SetError($"系统已存在该手机号");
            }
            if (!string.IsNullOrEmpty(input.Email))
            {
                var match = input.Email.MatchEmail();
                if (!match.isMatch)
                    return Unify.SetError($"邮箱格式错误");

                var any = await IsAnyAsync(it => it.Email == input.Email && it.Id != UserManager.UserId);//判断是否有重复的
                if (any)
                    return Unify.SetError($"系统已存在该邮箱");
            }

            //更新指定字段
            var result = await UpdateAsync(it => new SysUser
            {
                Email = input.Email,
                Phone = input.Phone,
            }, it => it.Id == UserManager.UserId);

            return default;
        }

        public void DeleteUserCache(string userId)
        {
            //用户角色信息
            _cacheService.HashDel<List<string>>(CacheConst.Cache_UserRelation, CacheConst.Field_UserHasApi(UserManager.UserId));
            _cacheService.HashDel<List<string>>(CacheConst.Cache_UserRelation, CacheConst.Field_UserHasMenu(UserManager.UserId));

            //用户Token信息
            foreach (var item in LoginDevices.AllowDevices)
            {
                var key = CacheConst.Cache_UserToken + userId + ":" + item;
                _cacheService.Remove(key);
            }
        }

        public async Task UpdateWorkbench(UpdateWorkbenchInput input)
        {
            //关系表保存个人工作台
            await _relationService.SaveRelation(CateGoryConst.Relation_SYS_USER_WORKBENCH_DATA, UserManager.UserId, null, input.WorkbenchData, true);
        }

        public async Task<string> GetLoginWorkbench()
        {
            //获取个人工作台信息
            var repo = ChangeRepository<DbRepository<SysRelation>>();
            var sysRelation = await repo.GetFirstAsync(x => x.Category == CateGoryConst.Relation_SYS_USER_WORKBENCH_DATA && x.ObjectId == UserManager.UserId);
            if (sysRelation != null)
            {
                //如果有数据直接返回个人工作台
                return sysRelation.ExtJson.ToLower();
            }
            else
            {
                //如果没数据去系统配置里取默认的工作台
                var devConfig = await _configService.GetByConfigKey(CateGoryConst.Config_SYS_BASE, "SYS_DEFAULT_WORKBENCH_DATA");
                if (devConfig != null)
                {
                    return devConfig.ConfigValue.ToLower();//返回工作台信息
                }
                else
                {
                    return "";
                }
            }
        }

        #region 私有方法

        private async Task<string> CheckInput(SysUser sysUser)
        {
            //判断账号重复,直接从redis拿
            var user = await GetFirstAsync(x => x.Account == sysUser.Account);
            if (user != null && user.Id != sysUser.Id)
                return ($"存在重复的账号:{sysUser.Account}");

            //如果手机号不是空
            if (!string.IsNullOrEmpty(sysUser.Phone))
            {
                if (!sysUser.Phone.MatchPhoneNumber())//验证手机格式
                    return ($"手机号码：{sysUser.Phone} 格式错误");
            }
            //如果邮箱不是空
            if (!string.IsNullOrEmpty(sysUser.Email))
            {
                var (ismatch, match) = sysUser.Email.MatchEmail();//验证邮箱格式
                if (!ismatch)
                    return ($"邮箱：{sysUser.Email} 格式错误");
            }
            return "OK";
        }

        private async Task<ISugarQueryable<SysUser>> GetQuery(UserPageInput input)
        {
            var searchkeys = input.SearchKey?.Split(',');

            var orgIds = await _sysOrgService.GetOrgChildIds(input.OrgId);//获取下级机构
            var query = Context.Queryable<SysUser>()
                .LeftJoin<SysOrg>((u, o) => u.OrgId == o.Id)
                .WhereIF(!string.IsNullOrEmpty(input.OrgId), u => orgIds.Contains(u.OrgId))//根据组织
                .WhereIF(input.Expression != null, input.Expression?.ToExpression())//动态查询
                .WhereIF(!string.IsNullOrEmpty(input.UserStatus), u => u.UserStatus == input.UserStatus)//根据状态查询
                .WhereIF(!string.IsNullOrEmpty(input.SearchKey), u => searchkeys.Contains(u.Name) || searchkeys.Contains(u.Account))//根据关键字查询
                .OrderByIF(!string.IsNullOrEmpty(input.SortField), $"u.{input.SortField} {input.SortOrder}")
                .OrderBy(u => u.Id)//排序
                .Select((u, o) => new SysUser
                {
                    Id = u.Id.SelectAll(),
                    OrgName = o.Name,
                    OrgNames = o.Names
                })
                .Mapper(u =>
                {
                    u.Password = null;//密码清空
                });
            return query;
        }

        #endregion
    }
}