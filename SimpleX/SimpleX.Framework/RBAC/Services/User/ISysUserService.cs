﻿namespace SimpleX.RBAC
{
    public interface ISysUserService : ITransient
    {
        Task<SysUser> GetSysUserByAccount(string account);

        Task<SysUser> GetSysUserById(string userId);

        Task SetLogined(string userId);

        Task<SqlSugarPagedList<SysUser>> Page(UserPageInput input);

        Task Add(UserAddInput input);

        Task Edit(UserEditInput input);

        Task Delete(List<BaseIdInput> input);

        Task EnableUser(BaseIdInput input);

        Task DisableUser(BaseIdInput input);

        Task ResetPassword(BaseIdInput input);

        Task GrantRole(UserGrantRoleInput input);

        Task<List<string>> OwnRole(BaseIdInput input);

        Task<List<string>> OwnButtonCodeList();

        Task<List<string>> OwnPermissionCodeList();

        Task<dynamic> UpdatePassword(UpdatePasswordInput input);

        Task<string> UpdateAvatar(BaseFileInput input);

        Task<dynamic> UpdateUserInfo(UpdateInfoInput input);

        Task UpdateWorkbench(UpdateWorkbenchInput input);

        Task<string> GetLoginWorkbench();
    }
}