﻿namespace SimpleX.RBAC
{
    [ApiExplorerSettings(GroupName = "RBAC")]
    [Route("sys/[controller]")]
    public class OrgController : BaseControllerSuperAdmin
    {
        private readonly ISysOrgService _sysOrgService;
        private readonly ISysUserService _sysUserService;

        public OrgController(ISysOrgService sysOrgService, ISysUserService sysUserService)
        {
            _sysOrgService = sysOrgService;
            _sysUserService = sysUserService;
        }

        /// <summary>
        /// 获取组织树
        /// </summary>
        /// <returns></returns>
        [HttpGet("tree")]
        [ActionPermission(ActionType.Query, "获取组织树")]
        public async Task<dynamic> Tree()
        {
            return await _sysOrgService.Tree();
        }

        /// <summary>
        /// 获取组织树选择器
        /// </summary>
        /// <returns></returns>
        [HttpGet("orgTreeSelector")]
        [ActionPermission(ActionType.Query, "获取组织树选择器")]
        public async Task<dynamic> OrgTreeSelector()
        {
            return await _sysOrgService.Tree();
        }

        /// <summary>
        /// 组织分页查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("page")]
        [ActionPermission(ActionType.Query, "组织分页查询")]
        public async Task<dynamic> Page([FromQuery] SysOrgPageInput input)
        {
            return await _sysOrgService.Page(input);
        }

        ///// <summary>
        ///// 获取用户选择器
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet("userSelector")]
        //public async Task<dynamic> UserSelector([FromQuery] UserSelectorInput input)
        //{
        //    return await _sysUserService.UserSelector(input);
        //}

        /// <summary>
        /// 添加组织
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        [ActionPermission(ActionType.Button, "添加组织")]
        public async Task Add([FromBody] SysOrgAddInput input)
        {
            await _sysOrgService.Add(input);
        }

        /// <summary>
        /// 修改组织
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("edit")]
        [ActionPermission(ActionType.Button, "修改组织")]
        public async Task Edit([FromBody] SysOrgEditInput input)
        {
            await _sysOrgService.Edit(input);
        }

        /// <summary>
        /// 删除组织
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>

        [HttpPost("delete")]
        [ActionPermission(ActionType.Button, "删除组织")]
        public async Task Delete([FromBody] List<BaseIdInput> input)
        {
            await _sysOrgService.Delete(input);
        }
    }
}