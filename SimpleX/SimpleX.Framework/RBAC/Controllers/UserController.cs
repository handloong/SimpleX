﻿namespace SimpleX.RBAC
{
    [ApiExplorerSettings(GroupName = "RBAC")]
    [Route("sys/[controller]")]
    public class UserController : BaseControllerSuperAdmin
    {
        private readonly ISysUserService _sysUserService;
        private readonly ISysOrgService _sysOrgService;
        private readonly IRoleService _roleService;

        public UserController(ISysUserService sysUserService,
            ISysOrgService sysOrgService,
            IRoleService roleService)
        {
            _sysUserService = sysUserService;
            _sysOrgService = sysOrgService;
            _roleService = roleService;
        }

        /// <summary>
        /// 用户分页查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("page")]
        [ActionPermission(ActionType.Query, "用户分页查询")]
        public async Task<dynamic> Page([FromQuery] UserPageInput input)
        {
            return await _sysUserService.Page(input);
        }

        /// <summary>
        /// 获取组织树选择器
        /// </summary>
        /// <returns></returns>
        [HttpGet("orgTreeSelector")]
        [ActionPermission(ActionType.Button, "获取组织树选择器")]
        public async Task<dynamic> OrgTreeSelector()
        {
            return await _sysOrgService.Tree();
        }

        /// <summary>
        /// 获取用户拥有角色
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("ownRole")]
        [ActionPermission(ActionType.Button, "获取用户拥有角色")]
        public async Task<dynamic> OwnRole([FromQuery] BaseIdInput input)
        {
            return await _sysUserService.OwnRole(input);
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        [ActionPermission(ActionType.Button, "添加用户")]
        public async Task Add([FromBody] UserAddInput input)
        {
            await _sysUserService.Add(input);
        }

        /// <summary>
        /// 修改用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("edit")]
        [ActionPermission(ActionType.Button, "修改用户")]
        public async Task Edit([FromBody] UserEditInput input)
        {
            await _sysUserService.Edit(input);
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        [ActionPermission(ActionType.Button, "删除用户")]
        public async Task Delete([FromBody] List<BaseIdInput> input)
        {
            await _sysUserService.Delete(input);
        }

        /// <summary>
        /// 禁用用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("disableUser")]
        [ActionPermission(ActionType.Button, "禁用用户")]
        public async Task DisableUser([FromBody] BaseIdInput input)
        {
            await _sysUserService.DisableUser(input);
        }

        /// <summary>
        /// 启用用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("enableUser")]
        [ActionPermission(ActionType.Button, "启用用户")]
        public async Task EnableUser([FromBody] BaseIdInput input)
        {
            await _sysUserService.EnableUser(input);
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("resetPassword")]
        [ActionPermission(ActionType.Button, "重置密码")]
        public async Task ResetPassword([FromBody] BaseIdInput input)
        {
            await _sysUserService.ResetPassword(input);
        }

        /// <summary>
        /// 给用户授权角色
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("grantRole")]
        [ActionPermission(ActionType.Button, "给用户授权角色")]
        public async Task GrantRole([FromBody] UserGrantRoleInput input)
        {
            await _sysUserService.GrantRole(input);
        }

        /// <summary>
        /// 获取角色选择器
        /// </summary>
        /// <returns></returns>
        [HttpGet("roleSelector")]
        [ActionPermission(ActionType.Button, "获取角色选择器")]
        public async Task<dynamic> RoleSelector(string searchKey)
        {
            return await _roleService.RoleSelector(searchKey);
        }
    }
}