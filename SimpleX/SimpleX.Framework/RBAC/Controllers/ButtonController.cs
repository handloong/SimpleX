﻿namespace SimpleX.RBAC;

[ApiExplorerSettings(GroupName = "System")]
[Route("sys/[controller]")]
public class ButtonController : BaseController
{
    private readonly IButtonService _buttonService;

    public ButtonController(IButtonService buttonService)
    {
        _buttonService = buttonService;
    }

    /// <summary>
    /// 按钮分页查询
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("page")]
    [ActionPermission(ActionType.Query, "按钮分页查询")]
    public async Task<dynamic> Page([FromQuery] ButtonPageInput input)
    {
        return await _buttonService.Page(input);
    }

    /// <summary>
    /// 添加按钮
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("add")]
    [ActionPermission(ActionType.Button, "添加按钮")]
    public async Task Add([FromBody] ButtonAddInput input)
    {
        await _buttonService.Add(input);
    }

    /// <summary>
    /// 修改按钮
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("edit")]
    [ActionPermission(ActionType.Button, "修改按钮")]
    public async Task Edit([FromBody] ButtonEditInput input)
    {
        await _buttonService.Edit(input);
    }

    /// <summary>
    /// 删除按钮
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("delete")]
    [ActionPermission(ActionType.Button, "删除按钮")]
    public async Task Delete([FromBody] List<BaseIdInput> input)
    {
        await _buttonService.Delete(input);
    }
}