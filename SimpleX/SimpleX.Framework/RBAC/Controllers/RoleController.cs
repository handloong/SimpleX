﻿namespace SimpleX.RBAC
{
    [ApiExplorerSettings(GroupName = "RBAC")]
    [Route("sys/[controller]")]
    public class RoleController : BaseControllerSuperAdmin
    {
        private readonly IRoleService _roleService;
        private readonly ISysMenuService _sysMenuService;

        public RoleController(IRoleService roleService,
            ISysMenuService sysMenuService)
        {
            _roleService = roleService;
            _sysMenuService = sysMenuService;
        }

        /// <summary>
        /// 角色分页查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("page")]
        [ActionPermission(ActionType.Query, "角色分页查询")]
        public async Task<dynamic> Page([FromQuery] RolePageInput input)
        {
            return await _roleService.Page(input);
        }

        /// <summary>
        /// 添加角色
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        [ActionPermission(ActionType.Button, "添加角色")]
        public async Task Add([FromBody] RoleAddInput input)
        {
            await _roleService.Add(input);
        }

        /// <summary>
        /// 修改角色
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("edit")]
        [ActionPermission(ActionType.Button, "修改角色")]
        public async Task Edit([FromBody] RoleEditInput input)
        {
            await _roleService.Edit(input);
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        [ActionPermission(ActionType.Button, "删除角色")]
        public async Task Delete([FromBody] List<BaseIdInput> input)
        {
            await _roleService.Delete(input);
        }

        /// <summary>
        /// 获取角色拥有资源
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("ownResource")]
        [ActionPermission(ActionType.Query, "获取角色拥有资源")]
        public async Task<dynamic> OwnResource([FromQuery] BaseIdInput input)
        {
            return await _roleService.OwnResource(input);
        }

        /// <summary>
        /// 给角色授权菜单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("grantResource")]
        [ActionPermission(ActionType.Button, "给角色授权菜单")]
        public async Task GrantMenu([FromBody] GrantMenuInput input)
        {
            await _roleService.GrantMenu(input);
        }

        /// <summary>
        /// 获取角色授权资源树
        /// </summary>
        /// <returns></returns>
        [HttpGet("resourceTreeSelector")]
        [ActionPermission(ActionType.Query, "获取角色授权资源树")]
        public async Task<dynamic> ResourceTreeSelector()
        {
            return await _sysMenuService.ResourceTreeSelector();
        }
    }
}