﻿namespace SimpleX.RBAC
{
    [ApiExplorerSettings(GroupName = "RBAC")]
    [Route("auth/[controller]")]
    public class UserCenterController : BaseControllerAuthorize
    {
        private readonly ISysMenuService _sysMenuService;
        private readonly ISysUserService _sysUserService;

        public UserCenterController(ISysMenuService sysMenuService,
            ISysUserService sysUserService)
        {
            _sysMenuService = sysMenuService;
            _sysUserService = sysUserService;
        }

        /// <summary>
        /// 获取当前登陆者的菜单信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("loginMenu")]
        [ActionPermission(ActionType.Button, "获取当前登陆者的菜单信息")]
        [IgnoreLog]
        public async Task<dynamic> LoginMenu()
        {
            return await _sysMenuService.GetOwnMenus();
        }

        /// <summary>
        /// 未读消息数
        /// </summary>
        /// <returns></returns>
        [HttpGet("UnReadCount")]
        [ActionPermission(ActionType.Button, "未读消息数")]
        public async Task<dynamic> UnReadCount()
        {
            return await Task.FromResult(0);
        }

        /// <summary>
        /// 获取个人工作台
        /// </summary>
        /// <returns></returns>
        [HttpGet("loginWorkbench")]
        [ActionPermission(ActionType.Button, "获取个人工作台")]
        public async Task<dynamic> LoginWorkbench()
        {
            return await _sysUserService.GetLoginWorkbench();
        }

        /// <summary>
        /// 编辑工作台
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("updateUserWorkbench")]
        [ActionPermission(ActionType.Button, "编辑工作台")]
        public async Task UpdateUserWorkbench([FromBody] UpdateWorkbenchInput input)
        {
            await _sysUserService.UpdateWorkbench(input);
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("updatePassword")]
        [ActionPermission(ActionType.Button, "修改密码")]
        public async Task UpdatePassword([FromBody] UpdatePasswordInput input)
        {
            await _sysUserService.UpdatePassword(input);
        }

        /// <summary>
        /// 修改头像
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("updateAvatar")]
        [ActionPermission(ActionType.Button, "修改头像")]
        public async Task<dynamic> UpdateAvatar([FromForm] BaseFileInput input)
        {
            return await _sysUserService.UpdateAvatar(input);
        }

        /// <summary>
        /// 编辑个人信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("updateUserInfo")]
        [ActionPermission(ActionType.Button, "编辑个人信息")]
        public async Task UpdateUserInfo([FromBody] UpdateInfoInput input)
        {
            await _sysUserService.UpdateUserInfo(input);
        }
    }
}