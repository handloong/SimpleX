﻿namespace SimpleX.RBAC;

[ApiExplorerSettings(GroupName = "System")]
[Route("sys/[controller]")]
public class ModuleController : BaseControllerSuperAdmin
{
    private readonly IModuleService _moduleService;

    public ModuleController(IModuleService moduleService)
    {
        _moduleService = moduleService;
    }

    /// <summary>
    /// 模块分页查询
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("page")]
    [ActionPermission(ActionType.Query, "模块分页查询")]
    public async Task<dynamic> Page([FromQuery] ModulePageInput input)
    {
        return await _moduleService.Page(input);
    }

    /// <summary>
    /// 添加模块
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("add")]
    [ActionPermission(ActionType.Button, "添加模块")]
    public async Task Add([FromBody] ModuleAddInput input)
    {
        await _moduleService.Add(input);
    }

    /// <summary>
    /// 修改模块
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("edit")]
    [ActionPermission(ActionType.Button, "修改模块")]
    public async Task Edit([FromBody] ModuleEditInput input)
    {
        await _moduleService.Edit(input);
    }

    /// <summary>
    /// 删除模块
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>

    [HttpPost("delete")]
    [ActionPermission(ActionType.Button, "删除模块")]
    public async Task Delete([FromBody] List<BaseIdInput> input)
    {
        await _moduleService.Delete(input);
    }
}