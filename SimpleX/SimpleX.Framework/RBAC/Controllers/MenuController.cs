﻿namespace SimpleX.RBAC
{
    [ApiExplorerSettings(GroupName = "RBAC")]
    [Route("sys/[controller]")]
    public class MenuController : BaseControllerSuperAdmin
    {
        private readonly ISysMenuService _sysMenuService;

        public MenuController(ISysMenuService sysMenuService)
        {
            _sysMenuService = sysMenuService;
        }

        /// <summary>
        /// 模块选择
        /// </summary>
        /// <returns></returns>
        [HttpGet("moduleSelector")]
        [ActionPermission(ActionType.Query, "模块选择")]
        public async Task<dynamic> ModuleSelector()
        {
            return await _sysMenuService.GetListByCategory(CateGoryConst.Menu_MODULE);
        }

        /// <summary>
        /// 获取菜单树
        /// </summary>
        /// <returns></returns>
        [HttpGet("tree")]
        [ActionPermission(ActionType.Query, "获取菜单树")]
        public async Task<dynamic> Tree([FromQuery] MenuTreeInput input)
        {
            return await _sysMenuService.Tree(input);
        }

        /// <summary>
        /// 获取菜单树选择器
        /// </summary>
        /// <returns></returns>
        [HttpGet("menuTreeSelector")]
        [ActionPermission(ActionType.Query, "获取菜单树选择器")]
        public async Task<dynamic> MenuTreeSelector([FromQuery] MenuTreeInput input)
        {
            return await _sysMenuService.Tree(input);
        }

        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        [ActionPermission(ActionType.Button, "添加菜单")]
        public async Task Add([FromBody] MenuAddInput input)
        {
            await _sysMenuService.Add(input);
        }

        /// <summary>
        /// 编辑菜单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("edit")]
        [ActionPermission(ActionType.Button, "编辑菜单")]
        public async Task Edit([FromBody] MenuEditInput input)
        {
            await _sysMenuService.Edit(input);
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        [ActionPermission(ActionType.Button, "删除菜单")]
        public async Task Delete([FromBody] List<BaseIdInput> input)
        {
            await _sysMenuService.Delete(input);
        }

        /// <summary>
        /// 更改模块
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("changeModule")]
        [ActionPermission(ActionType.Button, "更改模块")]
        public async Task ChangeModule([FromBody] MenuChangeModuleInput input)
        {
            await _sysMenuService.ChangeModule(input);
        }

        /// <summary>
        /// 获取菜单的所有接口权限信息(方法名+接口)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("GetAuthCodeAttributes")]
        [ActionPermission(ActionType.Query, "获取菜单的所有接口权限信息")]
        public dynamic GetAuthCodeAttributes([FromBody] MenuAuthInput input)
        {
            var retval = _sysMenuService.GetAuthCodeAttributes(input);
            if (retval.Count == 0)
                Unify.SetError($"该路由[{input.RouteName}]对应控制器中的方法没有标记AuthCode,请联系该功能对应开发DRI");

            return retval;
        }
    }
}