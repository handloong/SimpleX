namespace SimpleX.RBAC
{
    [ApiExplorerSettings(GroupName = "RBAC")]
    [Route("auth")]
    public class AuthController : BaseControllerAuthorize
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        /// <summary>
        /// 登陆
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("login")]
        [ActionPermission(ActionType.Button, "LoginIn")]//LoginIn用于日志分类
        public async Task<dynamic> Login(LoginInput input)
        {
            return await _authService.Login(input);
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        [HttpPost("loginOut")]
        [ActionPermission(ActionType.Button, "LoginOut")]//LoginOut用于日志分类
        public async Task LoginOut(LoginOutInput input)
        {
            await _authService.LoginOut(input);
        }

        /// <summary>
        /// 获取图片验证码
        /// </summary>
        /// <returns></returns>
        [HttpGet("getPicCaptcha")]
        [AllowAnonymous]
        [IgnoreLog]
        public dynamic GetPicCaptcha()
        {
            return default;
            //return _authService.GetCaptchaInfo();
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("getLoginUser")]
        [ActionPermission(ActionType.Button, "获取用户信息")]
        public async Task<dynamic> GetLoginUser()
        {
            return await _authService.GetLoginUser();
        }
    }
}