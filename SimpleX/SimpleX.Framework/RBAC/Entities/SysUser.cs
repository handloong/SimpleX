﻿namespace SimpleX.RBAC;

/// <summary>
/// 用户信息表
///</summary>
[SugarTable("sys_user", TableDescription = "用户信息表")]
[Tenant(SqlsugarConst.DB_Default)]
public class SysUser : BaseEntity
{
    /// <summary>
    /// 账号
    ///</summary>
    [SugarColumn(ColumnName = "Account", ColumnDescription = "账号", Length = 200, IsNullable = false)]
    public virtual string Account { get; set; }

    [SugarColumn(ColumnName = "Avatar", ColumnDescription = "头像", ColumnDataType = StaticConfig.CodeFirst_BigString, IsNullable = true)]
    public virtual string Avatar { get; set; }

    /// <summary>
    /// 密码
    ///</summary>
    [SugarColumn(ColumnName = "Password", ColumnDescription = "密码", Length = 500, IsNullable = false)]
    public string Password { get; set; }

    /// <summary>
    /// 机构id
    ///</summary>
    [SugarColumn(ColumnName = "OrgId", ColumnDescription = "机构id", IsNullable = false)]
    public virtual string OrgId { get; set; }

    /// <summary>
    /// 超级管理员
    ///</summary>
    [SugarColumn(ColumnName = "SuperAdmin", ColumnDescription = "超级管理员", Length = 2, IsNullable = false)]
    public string SuperAdmin { get; set; }

    /// <summary>
    /// 姓名
    ///</summary>
    [SugarColumn(ColumnName = "Name", ColumnDescription = "姓名", Length = 200, IsNullable = true)]
    public virtual string Name { get; set; }

    /// <summary>
    /// 手机
    ///</summary>
    [SugarColumn(ColumnName = "Phone", ColumnDescription = "手机", Length = 200, IsNullable = true)]
    public string Phone { get; set; }

    /// <summary>
    /// 邮箱
    ///</summary>
    [SugarColumn(ColumnName = "Email", ColumnDescription = "邮箱", Length = 200, IsNullable = true)]
    public string Email { get; set; }

    /// <summary>
    /// 上次登录ip
    ///</summary>
    [SugarColumn(ColumnName = "LastLoginIp", ColumnDescription = "上次登录ip", Length = 200, IsNullable = true)]
    public string LastLoginIp { get; set; }

    /// <summary>
    /// 上次登录时间
    ///</summary>
    [SugarColumn(ColumnName = "LastLoginTime", ColumnDescription = "上次登录时间", IsNullable = true)]
    public DateTime? LastLoginTime { get; set; }

    /// <summary>
    /// 用户状态
    ///</summary>
    [SugarColumn(ColumnName = "UserStatus", ColumnDescription = "用户状态", Length = 200, IsNullable = true)]
    public string UserStatus { get; set; }

    /// <summary>
    /// 认证类型
    ///</summary>
    [SugarColumn(ColumnName = "AuthenticationType", ColumnDescription = "认证类型")]
    public AuthenticationType AuthenticationType { get; set; }

    /// <summary>
    /// 机构信息
    /// </summary>
    [SugarColumn(IsIgnore = true)]
    public string OrgName { get; set; }

    /// <summary>
    /// 机构信息全称
    /// </summary>
    [SugarColumn(IsIgnore = true)]
    public string OrgNames { get; set; }

    ///// <summary>
    ///// 按钮码集合
    ///// </summary>
    //[SugarColumn(IsIgnore = true)]
    //public List<string> ButtonCodeList { get; set; }

    ///// <summary>
    ///// 权限码集合
    ///// </summary>
    //[SugarColumn(IsIgnore = true)]
    //public List<string> PermissionCodeList { get; set; }

    ///// <summary>
    ///// 角色ID集合
    ///// </summary>
    //[SugarColumn(IsIgnore = true)]
    //public List<string> RoleIdList { get; set; }
}

public enum AuthenticationType
{
    DataBase,
    LDAP
}