﻿namespace SimpleX.RBAC;

/// <summary>
/// 角色
///</summary>
[SugarTable("sys_role", TableDescription = "角色")]
[Tenant(SqlsugarConst.DB_Default)]
public class SysRole : BaseEntity
{
    /// <summary>
    /// 名称
    ///</summary>
    [SugarColumn(ColumnName = "Name", ColumnDescription = "名称", Length = 200, IsNullable = false)]
    public virtual string Name { get; set; }

    /// <summary>
    /// 分类
    ///</summary>
    [SugarColumn(ColumnName = "Category", ColumnDescription = "分类", Length = 200, IsNullable = false)]
    public virtual string Category { get; set; }

    /// <summary>
    /// 排序码
    ///</summary>
    [SugarColumn(ColumnName = "SortCode", ColumnDescription = "排序码", IsNullable = true)]
    public int? SortCode { get; set; }
}