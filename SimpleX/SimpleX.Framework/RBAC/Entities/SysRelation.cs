﻿namespace SimpleX.RBAC;

/// <summary>
/// 系统关系表
///</summary>
[SugarTable("sys_relation", TableDescription = "系统关系")]
[Tenant(SqlsugarConst.DB_Default)]
public class SysRelation : PrimaryKeyEntity
{
    /// <summary>
    /// 对象ID,USER HAS ROLE , UserId 就是 ObjectId, RoleId 就是 TargetId
    ///</summary>
    [SugarColumn(ColumnName = "ObjectId", ColumnDescription = "对象ID", IsNullable = false)]
    public string ObjectId { get; set; }

    /// <summary>
    /// 目标ID, USER HAS ROLE , UserId 就是 ObjectId, RoleId 就是 TargetId
    ///</summary>
    [SugarColumn(ColumnName = "TargetId", ColumnDescription = "目标ID", IsNullable = true)]
    public string TargetId { get; set; }

    /// <summary>
    /// 分类, USER HAS ROLE , UserId 就是 ObjectId, RoleId 就是 TargetId
    ///</summary>
    [SugarColumn(ColumnName = "Category", ColumnDescription = "分类", Length = 200, IsNullable = false)]
    public string Category { get; set; }
}