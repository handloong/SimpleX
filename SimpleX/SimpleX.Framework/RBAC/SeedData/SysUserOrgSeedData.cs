﻿namespace SimpleX.RBAC
{
    public class SysUserOrgSeedData : ISqlSugarEntitySeedData<SysOrg>
    {
        [IgnoreSeedDataUpdate]
        public IEnumerable<SysOrg> SeedData()
        {
            return SeedDataUtil.GetSeedData<SysOrg>(SqlsugarConst.DB_Default, "sys_org.json");
        }
    }
}