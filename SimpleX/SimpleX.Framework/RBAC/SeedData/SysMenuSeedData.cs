﻿namespace SimpleX.RBAC
{
    public class SysMenuSeedData : ISqlSugarEntitySeedData<SysMenu>
    {
        [IgnoreSeedDataUpdate]
        public IEnumerable<SysMenu> SeedData()
        {
            return SeedDataUtil.GetSeedData<SysMenu>(SqlsugarConst.DB_Default, "sys_menu.json");
        }
    }
}