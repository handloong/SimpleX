﻿namespace SimpleX.Sys
{
    public class DevDictSeedData : ISqlSugarEntitySeedData<DevDict>
    {
        [IgnoreSeedDataUpdate]
        public IEnumerable<DevDict> SeedData()
        {
            return SeedDataUtil.GetSeedData<DevDict>(SqlsugarConst.DB_Default, "dev_dict.json");
        }
    }
}