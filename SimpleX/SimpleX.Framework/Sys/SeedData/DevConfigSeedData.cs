﻿namespace SimpleX.Sys
{
    public class DevConfigSeedData : ISqlSugarEntitySeedData<DevConfig>
    {
        [IgnoreSeedDataUpdate]
        public IEnumerable<DevConfig> SeedData()
        {
            return SeedDataUtil.GetSeedData<DevConfig>(SqlsugarConst.DB_Default, "dev_config.json");
        }
    }
}