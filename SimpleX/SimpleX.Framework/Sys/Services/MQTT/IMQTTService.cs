﻿namespace SimpleX.Sys
{
    public interface IMQTTService : ITransient
    {
        Task<MQTTConfig> GetMQTTConfig();

        Task<bool> Publish(string topic, object data);
    }
}