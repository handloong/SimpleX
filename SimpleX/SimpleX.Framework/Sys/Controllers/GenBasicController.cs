﻿namespace SimpleX.Sys;

/// <summary>
/// 代码生成器
/// </summary>
[ApiExplorerSettings(GroupName = "System")]
[Route("gen/basic")]
public class GenBasicController : BaseControllerSuperAdmin
{
    private readonly IGenbasicService _genbasicService;

    public GenBasicController(IGenbasicService genbasicService)
    {
        _genbasicService = genbasicService;
    }

    /// <summary>
    /// 代码生成基础分页
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("page")]
    [ActionPermission(ActionType.Query, "代码生成基础分页")]
    public async Task<dynamic> Page([FromQuery] BasePageInput input)
    {
        return await _genbasicService.Page(input);
    }

    /// <summary>
    /// 获取所有表信息
    /// </summary>
    /// <returns></returns>
    [HttpGet("tables")]
    [ActionPermission(ActionType.Query, "获取所有表信息")]
    public dynamic Tables()
    {
        return _genbasicService.GetTables();
    }

    /// <summary>
    /// 获取项目所有程序集
    /// </summary>
    /// <returns></returns>
    [HttpGet("assemblies")]
    [ActionPermission(ActionType.Query, "获取项目所有程序集")]
    public dynamic GetAssemblies()
    {
        return _genbasicService.GetAssemblies();
    }

    /// <summary>
    /// 添加代码生成器
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("add")]
    [ActionPermission(ActionType.Button, "添加代码生成器")]
    public async Task<dynamic> Add([FromBody] GenBasicAddInput input)
    {
        return await _genbasicService.Add(input);
    }

    /// <summary>
    /// 编辑代码生成器
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("edit")]
    [ActionPermission(ActionType.Button, "编辑代码生成器")]
    public async Task<dynamic> Edit([FromBody] GenBasicEditInput input)
    {
        return await _genbasicService.Edit(input);
    }

    /// <summary>
    /// 删除代码生成配置
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("delete")]
    [ActionPermission(ActionType.Button, "删除代码生成配置")]
    public async Task Delete([FromBody] List<BaseIdInput> input)
    {
        await _genbasicService.Delete(input);
    }

    /// <summary>
    /// 代码生成预览
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("previewGen")]
    [ActionPermission(ActionType.Button, "代码生成预览")]
    public async Task<dynamic> PreviewGen([FromQuery] BaseIdInput input)
    {
        return await _genbasicService.PreviewGen(input);
    }

    /// <summary>
    /// 生成指定表的种子数据
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("WriteSeedDataJson")]
    [ActionPermission(ActionType.Button, "WriteSeedDataJson")]
    public async Task WriteSeedDataJson([FromBody] BaseIdsInput input)
    {
        await _genbasicService.WriteSeedDataJson(input);
    }

    /// <summary>
    /// 执行代码生成(本地)
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("execGenPro")]
    [ActionPermission(ActionType.Button, "执行代码生成(本地)")]
    public async Task ExecGenPro([FromBody] BaseIdInput input)
    {
        await _genbasicService.ExecGenPro(input);
    }

    /// <summary>
    /// 执行代码生成(压缩包)
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("execGenZip")]
    [ActionPermission(ActionType.Button, "执行代码生成(压缩包)")]
    public async Task<IActionResult> ExecGenZip([FromQuery] BaseIdInput input)
    {
        return await _genbasicService.ExecGenZip(input);
    }
}