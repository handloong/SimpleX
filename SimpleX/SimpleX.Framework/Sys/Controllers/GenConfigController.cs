﻿using SimpleX.Sys;

/// <summary>
/// 代码生成配置控制器
/// </summary>
[ApiExplorerSettings(GroupName = "System")]
[Route("gen/config")]
public class GenConfigController : BaseControllerSuperAdmin
{
    private readonly IGenConfigService _genConfigService;

    public GenConfigController(IGenConfigService genConfigService)
    {
        _genConfigService = genConfigService;
    }

    /// <summary>
    /// 查询代码生成详细配置列表
    /// </summary>
    /// <param name="basicId"></param>
    /// <returns></returns>
    [HttpGet("list")]
    [ActionPermission(SimpleX.ActionType.Query, "查询代码生成详细配置列表")]
    public async Task<dynamic> List(string basicId)
    {
        return await _genConfigService.List(basicId);
    }

    /// <summary>
    /// 编辑代码生成详细
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("editBatch")]
    [ActionPermission(SimpleX.ActionType.Button, "编辑代码生成详细")]
    public async Task EditBatch([FromBody] List<GenConfig> input)
    {
        await _genConfigService.EditBatch(input);
    }
}