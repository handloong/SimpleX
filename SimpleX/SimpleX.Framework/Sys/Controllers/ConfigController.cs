﻿namespace SimpleX.Sys;

[ApiExplorerSettings(GroupName = "System")]
[Route("dev/[controller]")]
public class ConfigController : BaseControllerSuperAdmin
{
    private readonly IConfigService _configService;//系统配置服务

    public ConfigController(IConfigService configService)
    {
        _configService = configService;
    }

    /// <summary>
    /// 获取系统基础配置
    /// </summary>
    /// <returns></returns>
    [HttpGet("sysBaseList")]
    [AllowAnonymous]
    [ActionPermission(ActionType.Query, "获取系统基础配置")]
    public async Task<List<DevConfig>> SysBaseList()
    {
        var configList = await _configService.GetListByCategory(CateGoryConst.Config_SYS_BASE);
        configList.Add(new DevConfig
        {
            Category = CateGoryConst.Config_SYS_BASE,
            ConfigKey = "SYS_RSA_PUBLICKEY",
            ConfigValue = PwdUtils.PublicKey
        });

        configList.Add(new DevConfig
        {
            Category = CateGoryConst.Config_SYS_BASE,
            ConfigKey = "SYS_WEBAPI_VERSION",
            ConfigValue = Assembly.GetExecutingAssembly().GetName().Version.ToString()
        });
        return configList;
    }

    /// <summary>
    /// 获取PDA基础配置
    /// </summary>
    /// <returns></returns>
    [HttpGet("pdaList")]
    [AllowAnonymous]
    [ActionPermission(ActionType.Query, "获取PDA基础配置")]
    public async Task<List<DevConfig>> PDAList()
    {
        return await _configService.GetListByCategory(CateGoryConst.Config_PDA);
    }

    /// <summary>
    /// 获取配置列表
    /// </summary>
    /// <param name="category">分类</param>
    /// <returns></returns>
    [HttpGet("list")]
    [ActionPermission(ActionType.Query, "获取配置列表")]
    public async Task<dynamic> List(string category)
    {
        return await _configService.GetListByCategory(category);
    }

    /// <summary>
    /// 配置分页
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("page")]
    [ActionPermission(ActionType.Query, "配置分页")]
    public async Task<dynamic> Page([FromQuery] ConfigPageInput input)
    {
        return await _configService.Page(input);
    }

    /// <summary>
    /// 添加配置
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("add")]
    [ActionPermission(ActionType.Button, "添加配置")]
    public async Task Add([FromBody] ConfigAddInput input)
    {
        await _configService.Add(input);
    }

    /// <summary>
    /// 修改配置
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("edit")]
    [ActionPermission(ActionType.Button, "修改配置")]
    public async Task Edit([FromBody] ConfigEditInput input)
    {
        await _configService.Edit(input);
    }

    /// <summary>
    /// 删除配置
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>

    [HttpPost("delete")]
    [ActionPermission(ActionType.Button, "删除配置")]
    public async Task Delete([FromBody] ConfigDeleteInput input)
    {
        await _configService.Delete(input);
    }

    /// <summary>
    /// 配置批量更新
    /// </summary>
    /// <returns></returns>
    [HttpPost("editBatch")]
    [ActionPermission(ActionType.Button, "配置批量更新")]
    public async Task EditBatch([FromBody] List<DevConfig> devConfigs)
    {
        await _configService.EditBatch(devConfigs);
    }
}