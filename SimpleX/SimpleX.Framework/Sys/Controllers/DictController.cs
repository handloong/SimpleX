﻿namespace SimpleX.Sys;

[ApiExplorerSettings(GroupName = "System")]
[Route("dev/[controller]")]
public class DictController : BaseControllerSuperAdmin
{
    private readonly IDictService _dictService;

    public DictController(IDictService dictService)
    {
        _dictService = dictService;
    }

    /// <summary>
    /// 获取字典树
    /// </summary>
    /// <returns></returns>
    [HttpGet("tree")]
    [IgnoreSuperAdmin]
    [ActionPermission(ActionType.Query, "获取字典树")]
    public async Task<dynamic> Tree([FromQuery] DictTreeInput input)
    {
        return await _dictService.Tree(input);
    }

    /// <summary>
    /// 字典分页查询
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("page")]
    [ActionPermission(ActionType.Query, "字典分页查询")]
    public async Task<dynamic> Page([FromQuery] DictPageInput input)
    {
        return await _dictService.Page(input);
    }

    /// <summary>
    /// 添加字典
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("add")]
    [ActionPermission(ActionType.Button, "添加字典")]
    public async Task Add([FromBody] DictAddInput input)
    {
        await _dictService.Add(input);
    }

    /// <summary>
    /// 修改字典
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("edit")]
    [ActionPermission(ActionType.Button, "修改字典")]
    public async Task Edit([FromBody] DictEditInput input)
    {
        await _dictService.Edit(input);
    }

    /// <summary>
    /// 删除字典
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("delete")]
    [ActionPermission(ActionType.Button, "删除字典")]
    public async Task Delete([FromBody] DictDeleteInput input)
    {
        await _dictService.Delete(input);
    }
}