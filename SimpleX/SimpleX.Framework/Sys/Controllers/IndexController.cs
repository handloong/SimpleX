﻿namespace SimpleX.WebApi.Controllers.System;

[ApiExplorerSettings(GroupName = "System")]
[Route("sys/[controller]")]
public class IndexController : BaseControllerAuthorize
{
    /// <summary>
    /// 获取当前用户访问日志列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("visLog/list")]
    [IgnoreLog]
    public async Task<dynamic> VisLogList()
    {
        await Task.CompletedTask;
        return null;
    }

    /// <summary>
    /// 获取当前用户操作日志列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("opLog/list")]
    [IgnoreLog]
    public async Task<dynamic> OpLogList()
    {
        await Task.CompletedTask;
        return null;
    }

    /// <summary>
    /// 获取当前用户站内信列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("message/list")]
    [IgnoreLog]
    public async Task<dynamic> MessageList()
    {
        await Task.CompletedTask;
        return Array.Empty<string>();
    }

    /// <summary>
    /// 获取当前用户日程列表
    /// </summary>
    /// <returns></returns>
    [HttpGet("schedule/list")]
    [IgnoreLog]
    public async Task<dynamic> ScheduleList([FromQuery] dynamic input)
    {
        await Task.CompletedTask;
        return null;
    }

    /// <summary>
    /// 添加日程
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("schedule/add")]
    public async Task AddSchedule([FromBody] dynamic input)
    {
        await Task.CompletedTask;
        Unify.SetError("开发中");
    }

    /// <summary>
    /// 删除日程
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("schedule/deleteSchedule")]
    public async Task DeleteSchedule([FromBody] List<BaseIdInput> input)
    {
        await Task.CompletedTask;
    }
}