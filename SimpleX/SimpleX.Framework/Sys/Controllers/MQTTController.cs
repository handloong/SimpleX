﻿using SimpleX.Sys;

namespace SimpleX.WebApi.Controllers.System;

[ApiExplorerSettings(GroupName = "System")]
[Route("mqtt")]
public class MQTTController : BaseControllerAuthorize
{
    private readonly IMQTTService _mQTTService;

    public MQTTController(IMQTTService mQTTService)
    {
        _mQTTService = mQTTService;
    }

    /// <summary>
    /// 获取当前mqtt配置信息
    /// </summary>
    /// <returns></returns>
    [HttpGet("getParameter")]
    [IgnoreLog]
    public async Task<dynamic> ScheduleList([FromQuery] dynamic input)
    {
        var config = await _mQTTService.GetMQTTConfig();
        return new
        {
            ClientId = $"SPX_{UserManager.UserAccount}_{UserManager.Device}",
            Url = config?.Url,
            UserName = config?.UserName,
            Password = config?.Password,
            Topics = new List<string> { $"SPXUI/{UserManager.UserId}" }
        };
    }

    /// <summary>
    /// 获取当前mqtt配置信息
    /// </summary>
    /// <returns></returns>
    [HttpGet("Publish")]
    [AllowAnonymous]
    public async Task<dynamic> Publish([FromQuery] dynamic input)
    {
        var config = await _mQTTService.Publish("abc", "123");
        return "ok";
    }
}