﻿namespace SimpleX.Sys;

/// <summary>
/// 代码生成绑定视图
/// </summary>
public class GenViewModel : GenBasic
{
    #region 基础

    /// <summary>
    /// 生成时间
    /// </summary>
    public string GenTime { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

    /// <summary>
    /// 表字段
    /// </summary>
    public List<GenConfig> TableFields { get; set; }

    #endregion 基础

    #region 菜单

    /// <summary>
    /// 菜单ID
    /// </summary>
    public string MenuId { get; set; } = IDUtils.GetId();

    /// <summary>
    /// 菜单编码
    /// </summary>
    public string MenuCode { get; set; } = $"MENU{IDUtils.GetId()}";

    /// <summary>
    /// /菜单路径
    /// </summary>
    public string MenuPath
    {
        get { return $"/{RouteName}/{BusName}"; }
    }

    /// <summary>
    /// /菜单路径
    /// </summary>
    public string MenuComponent
    {
        get { return $"{RouteName}/{BusName}/index"; }
    }

    #endregion 菜单

    #region 按钮

    /// <summary>
    /// 添加按钮ID
    /// </summary>
    public string AddButtonId { get; set; } = IDUtils.GetId();

    /// <summary>
    /// 批量删除按钮ID
    /// </summary>
    public string BatchDeleteButtonId { get; set; } = IDUtils.GetId();

    /// <summary>
    /// 编辑按钮ID
    /// </summary>
    public string EditButtonId { get; set; } = IDUtils.GetId();

    /// <summary>
    /// 删除按钮ID
    /// </summary>
    public string DeleteButtonId { get; set; } = IDUtils.GetId();

    #endregion 按钮

    #region 后端

    /// <summary>
    /// 类名首字母小写
    /// </summary>
    public string ClassNameFirstLower
    {
        get { return StringHelper.FirstCharToLower(ClassName); }
    }

    /// <summary>
    /// swagger分组名称
    /// </summary>
    public string ApiGroup
    {
        get
        {
            return ServicePosition.Split(".").Last();
        }
    }

    /// <summary>
    /// 服务名
    /// </summary>
    public string ServiceName
    {
        get { return ClassName + "Service"; }
    }

    /// <summary>
    /// 服务名开头首字母小写
    /// </summary>
    public string ServiceFirstLower
    {
        get { return ClassNameFirstLower + "Service"; }
    }

    #endregion 后端

    #region 注释描述

    /// <summary>
    /// 分页查询
    /// </summary>
    public string DescriptionPage
    {
        get { return FunctionName + "分页查询"; }
    }

    /// <summary>
    /// 添加
    /// </summary>
    public string DescriptionAdd
    {
        get { return "添加" + FunctionName; }
    }

    /// <summary>
    /// 修改
    /// </summary>
    public string DescriptionEdit
    {
        get { return "修改" + FunctionName; }
    }

    /// <summary>
    /// 修改
    /// </summary>
    public string DescriptionDelete
    {
        get { return "删除" + FunctionName; }
    }

    /// <summary>
    /// 修改
    /// </summary>
    public string DescriptionDetail
    {
        get { return FunctionName + "详情"; }
    }

    #endregion 注释描述

    #region 参数

    /// <summary>
    /// 分页参数
    /// </summary>
    public string PageInput
    { get { return ClassName + "PageInput"; } }

    /// <summary>
    /// 添加参数
    /// </summary>
    public string AddInput
    { get { return ClassName + "AddInput"; } }

    /// <summary>
    /// 编辑参数
    /// </summary>
    public string EditInput
    { get { return ClassName + "EditInput"; } }

    #endregion 参数

    #region 权限编码

    /// <summary>
    /// 权限编码
    /// </summary>
    public string Perm
    {
        get
        {
            var ret = $"{RouteName}:{BusName}:";
            return ret.ToLower();
        }
    }

    #endregion
}