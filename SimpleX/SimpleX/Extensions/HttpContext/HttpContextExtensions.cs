﻿namespace SimpleX
{
    public static class HttpContextExtensions
    {
        /// <summary>
        /// 获取 Action 特性
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public static TAttribute GetMetadata<TAttribute>(this HttpContext httpContext)
            where TAttribute : class
        {
            return httpContext.GetEndpoint()?.Metadata?.GetMetadata<TAttribute>();
        }

        /// <summary>
        /// 获取 Token
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public static string GetToken(this HttpContext httpContext)
        {
            var token = Convert.ToString(httpContext.Request.Headers.Authorization).Replace("Bearer ", "");
            return token;
        }
    }
}