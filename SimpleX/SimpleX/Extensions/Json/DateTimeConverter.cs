﻿//using System.Globalization;

//namespace SimpleX
//{
//    public class DateTimeConverter : System.Text.Json.Serialization.JsonConverter<DateTime>
//    {
//        private const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
//        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
//        {
//            if (reader.TokenType != JsonTokenType.String)
//            {
//                throw new JsonException($"Expected token type 'String' but got '{reader.TokenType}'");
//            }

//            var dateString = reader.GetString();
//            if (DateTime.TryParseExact(dateString, DateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTime))
//            {
//                return dateTime;
//            }
//            else
//            {
//                throw new JsonException($"Invalid datetime format: {dateString}");
//            }
//        }

//        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
//        {
//            writer.WriteStringValue(value.ToString(DateTimeFormat, CultureInfo.InvariantCulture));
//        }
//    }
//}