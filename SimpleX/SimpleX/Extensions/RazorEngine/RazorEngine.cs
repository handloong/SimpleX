﻿using RazorEngineCore;
using System.Threading;

namespace SimpleX
{
    public static class RazorEngineExtension
    {
        public static async Task<string> CompileRunAsync<T>(this IRazorEngine engine, string content, T model, Action<IRazorEngineCompilationOptionsBuilder> builderAction = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var template = await engine.CompileAsync<RazorModel<T>>(content, builder =>
            {
                builderAction?.Invoke(builder);
            });

            var result = await template.RunAsync(x => x.Model = model);

            return result;
        }
    }

    public class RazorModel<T> : RazorEngineTemplateBase
    {
        public new T Model { get; set; }
    }
}