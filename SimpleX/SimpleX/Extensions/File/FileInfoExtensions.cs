﻿using Microsoft.AspNetCore.Mvc;
using System.IO;

namespace SimpleX
{
    public static class FileInfoExtensions
    {
        /// <summary>
        /// 下载文件
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public static FileStreamResult ToFileStreamResult(this FileInfo fileInfo)
        {
            return new FileStreamResult(new FileStream(fileInfo.FullName, FileMode.Open), "application/octet-stream")
            {
                FileDownloadName = fileInfo.Name,
            };
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public static FileStreamResult ToFileStreamResult(this FileInfo fileInfo, string filedoenLoadName)
        {
            return new FileStreamResult(new FileStream(fileInfo.FullName, FileMode.Open), "application/octet-stream")
            {
                FileDownloadName = filedoenLoadName,
            };
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public static FileStreamResult ToFileStreamResult(this byte[] data, string filedoenLoadName)
        {
            return new FileStreamResult(new MemoryStream(data), "application/octet-stream")
            {
                FileDownloadName = filedoenLoadName,
            };
        }
    }
}