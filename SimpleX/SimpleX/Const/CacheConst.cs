﻿namespace SimpleX
{
    public class CacheConst
    {
        public const string Cache_Prefix_Web = "SimpleXWebApi:";

        public const string Cache_UserToken = Cache_Prefix_Web + "UserToken:";

        /// <summary>
        /// 字典表缓存Key
        /// </summary>
        public const string Cache_DevDict = Cache_Prefix_Web + "DevDict";

        /// <summary>
        /// 登录验证码缓存Key
        /// </summary>
        public const string Cache_Captcha = Cache_Prefix_Web + "Captcha:";

        /// <summary>
        /// 系统配置表缓存Key
        /// </summary>
        public const string Cache_DevConfig = Cache_Prefix_Web + "DevConfig:";

        /// <summary>
        /// Hash 用户对应角色->对应API权限 [编辑角色删除整个Key,编辑用户删除单个field]
        /// </summary>

        public const string Cache_UserRelation = Cache_Prefix_Web + "UserRelation";

        /// <summary>
        /// Hash Field
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string Field_UserHasApi(string userId) => $"API_" + userId;

        /// <summary>
        /// Hash Field
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string Field_UserHasMenu(string userId) => $"MENU_" + userId;
    }
}