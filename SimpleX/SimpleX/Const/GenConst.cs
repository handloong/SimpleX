﻿namespace SimpleX;

/// <summary>
/// 代码生成器常量
/// </summary>
public class GenConst
{
    #region 是否

    /// <summary>
    /// 是
    /// </summary>
    public const string Yes = "Y";

    /// <summary>
    /// 否
    /// </summary>
    public const string No = "N";

    #endregion 是否

    /// <summary>
    /// 压缩包
    /// </summary>
    public const string Zip = "ZIP";

    /// <summary>
    /// 项目中
    /// </summary>
    public const string Pro = "PRO";
}