﻿namespace SimpleX;

/// <summary>
/// 用户常量
/// </summary>
public class ClaimConst
{
    /// <summary>
    /// 用户Id
    /// </summary>
    public const string UserId = "UserId";

    /// <summary>
    /// 工号(登陆账号)
    /// </summary>
    public const string Account = "Account";

    /// <summary>
    /// 姓名
    /// </summary>
    public const string Name = "Name";

    /// <summary>
    /// 超级管理员
    /// </summary>
    public const string SuperAdmin = "SXA";

    /// <summary>
    /// 登陆设备
    /// </summary>
    public const string Device = "Device";
}