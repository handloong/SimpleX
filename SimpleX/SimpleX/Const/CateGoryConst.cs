﻿namespace SimpleX;

/// <summary>
/// 分类常量
/// </summary>
public class CateGoryConst
{
    #region 系统配置

    /// <summary>
    /// 系统基础
    /// </summary>
    public const string Config_SYS_BASE = "SYS_BASE";

    /// <summary>
    /// 业务定义
    /// </summary>
    public const string Config_BIZ_DEFINE = "BIZ_DEFINE";

    /// <summary>
    /// 文件-本地
    /// </summary>
    public const string Config_FILE_LOCAL = "FILE_LOCAL";

    /// <summary>
    /// 文件-MINIO
    /// </summary>
    public const string Config_FILE_MINIO = "FILE_MINIO";

    #endregion 系统配置

    #region Mqtt配置

    /// <summary>
    /// MQTT配置
    /// </summary>
    public const string Config_MQTT_BASE = "MQTT_BASE";

    #endregion Mqtt配置

    #region Mqtt配置

    /// <summary>
    /// PDA配置
    /// </summary>
    public const string Config_PDA = "PDA_UPDATE";

    #endregion Mqtt配置

    #region 关系表

    /// <summary>
    /// 用户有哪些角色
    /// </summary>
    public const string Relation_SYS_USER_HAS_ROLE = "SYS_USER_HAS_ROLE";

    /// <summary>
    /// 角色有哪些菜单
    /// </summary>
    public const string Relation_SYS_ROLE_HAS_MENU = "SYS_ROLE_HAS_MENU";

    /// <summary>
    ///用户有哪些额外的菜单
    /// </summary>
    public const string Relation_SYS_USER_HAS_MENU = "SYS_USER_HAS_MENU";

    /// <summary>
    /// 用户工作台数据
    /// </summary>
    public const string Relation_SYS_USER_WORKBENCH_DATA = "SYS_USER_WORKBENCH_DATA";

    /// <summary>
    /// 用户日程数据
    /// </summary>
    public const string Relation_SYS_USER_SCHEDULE_DATA = "SYS_USER_SCHEDULE_DATA";

    /// <summary>
    /// 站内信与接收用户
    /// </summary>
    public const string Relation_MSG_TO_USER = "MSG_TO_USER";

    #endregion 关系表

    #region 菜单表

    /// <summary>
    /// 模块
    /// </summary>
    public const string Menu_MODULE = "MODULE";

    /// <summary>
    /// 菜单
    /// </summary>
    public const string Menu_MENU = "MENU";

    /// <summary>
    /// 目录
    /// </summary>
    public const string Menu_CATALOG = "CATALOG";

    /// <summary>
    /// 单页
    /// </summary>
    public const string Menu_SPA = "SPA";

    /// <summary>
    /// 按钮
    /// </summary>
    public const string Menu_BUTTON = "BUTTON";

    /// <summary>
    /// 内链
    /// </summary>
    public const string IFRAME = "IFRAME";

    /// <summary>
    /// 外链
    /// </summary>
    public const string LINK = "LINK";

    #endregion 资源表

    #region 日志表

    /// <summary>
    /// 登录
    /// </summary>
    public const string Log_LOGIN = "LOGIN";

    /// <summary>
    /// 登出
    /// </summary>
    public const string Log_LOGOUT = "LOGOUT";

    /// <summary>
    /// 操作
    /// </summary>
    public const string Log_OPERATE = "OPERATE";

    /// <summary>
    /// 异常
    /// </summary>
    public const string Log_EXCEPTION = "EXCEPTION";

    #endregion 日志表

    #region 字典表

    /// <summary>
    /// 框架
    /// </summary>
    public const string Dict_FRM = "FRM";

    /// <summary>
    /// 业务
    /// </summary>
    public const string Dict_BIZ = "BIZ";

    #endregion 字典表

    #region 组织表

    /// <summary>
    /// 部门
    /// </summary>
    public const string Org_DEPT = "DEPT";

    /// <summary>
    /// 公司
    /// </summary>
    public const string Org_COMPANY = "COMPANY";

    #endregion 组织表

    #region 站内信表

    /// <summary>
    /// 通知
    /// </summary>
    public const string Message_INFORM = "INFORM";

    /// <summary>
    /// 公告
    /// </summary>
    public const string Message_NOTICE = "NOTICE";

    #endregion 站内信表
}