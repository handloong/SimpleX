﻿namespace SimpleX
{
    public interface IScoped
    { }

    public interface ITransient
    { }

    public interface ISingleton
    { }
}