﻿namespace SimpleX
{
    public static partial class ServicesExtension
    {
        /// <summary>
        /// 自动扫描并注入继承IScoped/ISingleton/ITransient的接口或者类
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddInjectAssembly(this IServiceCollection services)
        {
            var assemblys = App.Assemblies;

            return services.Scan(scan =>
            {
                scan.FromAssemblies(assemblys)
                     //接口
                     .AddClasses(classes => classes.AssignableTo(typeof(IScoped)))
                         .AsImplementedInterfaces()
                         .WithScopedLifetime()
                     .AddClasses(classes => classes.AssignableTo(typeof(ISingleton)))
                           .AsImplementedInterfaces()
                           .WithSingletonLifetime()
                     .AddClasses(classes => classes.AssignableTo(typeof(ITransient)))
                           .AsImplementedInterfaces()
                           .WithTransientLifetime()

                     //类
                     .AddClasses(classes => classes.AssignableTo(typeof(IScoped)))
                           .AsSelf()
                           .WithScopedLifetime()
                     .AddClasses(classes => classes.AssignableTo(typeof(ISingleton)))
                           .AsSelf()
                           .WithSingletonLifetime()
                     .AddClasses(classes => classes.AssignableTo(typeof(ITransient)))
                           .AsSelf()
                           .WithTransientLifetime();
            });
        }
    }
}