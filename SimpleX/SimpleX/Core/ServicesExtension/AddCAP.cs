﻿using Savorboard.CAP.InMemoryMessageQueue;

namespace SimpleX
{
    public static partial class ServicesExtension
    {
        public static IServiceCollection AddCAP(this IServiceCollection services)
        {
            //https://cap.dotnetcore.xyz/user-guide/en/transport/in-memory-queue/

            services.AddCap(x =>
            {
                x.UseInMemoryMessageQueue();
                x.UseInMemoryStorage();

                //消息重试的最大次数
                x.FailedRetryCount = 3;
                //消息重试间隔时间,4min后该值设置生效（默认快速重试3次)
                x.FailedRetryInterval = 60;
                //发送成功的消息的过期时间(过期则删除)
                x.SucceedMessageExpiredAfter = 24 * 3600;
                //发送消息失败后的回调
                x.FailedThresholdCallback = (context) =>
                {
                    //通知管理人员或其它逻辑
                };
            });

            return services;
        }
    }
}