﻿namespace SimpleX
{
    public static partial class ServicesExtension
    {
        public static IServiceCollection AddCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                        builder =>
                        {
                            builder.AllowAnyOrigin()
                           .SetPreflightMaxAge(TimeSpan.FromHours(1))
                            .AllowAnyHeader().AllowAnyMethod();
                        });
            });

            return services;
        }
    }
}