﻿namespace SimpleX
{
    public class Ex
    {
        /// <summary>
        /// 业务异常
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static Exception Business(string message)
        {
            return new BusinessException(message);
        }
    }

    public class BusinessException : Exception
    {
        public BusinessException(string message) : base(message)
        {
        }
    }
}