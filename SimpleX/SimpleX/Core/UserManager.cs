﻿namespace SimpleX
{
    public class UserManager
    {
        /// <summary>
        /// 当前用户Id
        /// </summary>
        public static string UserId => App.User?.FindFirst(ClaimConst.UserId)?.Value;

        /// <summary>
        /// 当前用户账号
        /// </summary>
        public static string UserAccount => App.User?.FindFirst(ClaimConst.Account)?.Value;

        /// <summary>
        /// 当前用户昵称
        /// </summary>
        public static string Name => App.User?.FindFirst(ClaimConst.Name)?.Value;

        /// <summary>
        /// 是否超级管理员
        /// </summary>
        public static bool SuperAdmin => (App.User?.FindFirst(ClaimConst.SuperAdmin)?.Value) == "Y";

        /// <summary>
        /// 登陆设备
        /// </summary>
        public static string Device => App.User?.FindFirst(ClaimConst.Device)?.Value;
    }
}