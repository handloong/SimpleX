﻿namespace SimpleX
{
    public class WebsocketMessage<T>
    {
        public string Action { get; set; }

        public T Data { get; set; }
    }
}