﻿using System.Net.WebSockets;
using System.Threading;

namespace SimpleX
{
    public class WebsocketClient
    {
        public WebSocket WebSocket { get; set; }

        public string ClientId { get; set; }

        public Task SendMessageAsync(string message)
        {
            var msg = Encoding.UTF8.GetBytes(message);
            return WebSocket.SendAsync(new ArraySegment<byte>(msg, 0, msg.Length), WebSocketMessageType.Text, true, CancellationToken.None);
        }
    }
}