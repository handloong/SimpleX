﻿using UAParser;

namespace SimpleX.Middlewares
{
    public class RequestModel
    {
        public string RequestName { get; set; }
        public string RequestBody { get; set; }
        public string QueryString { get; set; }
        public string Path { get; set; }
        public string RequestHeaders { get; set; }
        public string CodeMethodName { get; set; }
        public string ClassName { get; set; }
        public string HttpMethodName { get; set; }
        public string IpAddress { get; set; }
        public DateTime StartTime { get; set; }
        public string UserId { get; set; }
        public string Account { get; set; }
        public string UserName { get; set; }
        public ClientInfo ClientInfo { get; set; }
    }
}