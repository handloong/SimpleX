﻿using SqlSugar;

namespace SimpleX.Middlewares
{
    /// <summary>
    /// 请求记录
    /// </summary>
    [SugarTable("dev_log_operate")]
    public class FullModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [SugarColumn(ColumnDescription = "Id", IsPrimaryKey = true)]
        public string Id { get; set; } = IDUtils.GetId();

        /// <summary>
        /// 日志分类
        ///</summary>
        [SugarColumn(ColumnName = "Category", ColumnDescription = "日志分类", Length = 200, IsNullable = true)]
        public string Category { get; set; }

        /// <summary>
        /// 日志名称
        ///</summary>
        [SugarColumn(ColumnName = "Name", ColumnDescription = "日志名称", Length = 200, IsNullable = true)]
        public string Name { get; set; }

        /// <summary>
        /// 返回Http状态码
        ///</summary>
        [SugarColumn(ColumnName = "ResponseStatus", ColumnDescription = "执行状态", Length = 200, IsNullable = true)]
        public int ResponseStatus { get; set; }

        /// <summary>
        /// 操作ip
        ///</summary>
        [SugarColumn(ColumnName = "OpIp", ColumnDescription = "操作ip", Length = 200, IsNullable = true)]
        public string OpIp { get; set; }

        /// <summary>
        /// 操作地址
        ///</summary>
        [SugarColumn(ColumnName = "OpAddress", ColumnDescription = "操作地址", Length = 200, IsNullable = true)]
        public string OpAddress { get; set; }

        /// <summary>
        /// 操作浏览器
        ///</summary>
        [SugarColumn(ColumnName = "OpBrowser", ColumnDescription = "操作浏览器", Length = 200, IsNullable = true)]
        public string OpBrowser { get; set; }

        /// <summary>
        /// 操作系统
        ///</summary>
        [SugarColumn(ColumnName = "OpOs", ColumnDescription = "操作系统", Length = 200, IsNullable = true)]
        public string OpOs { get; set; }

        /// <summary>
        /// 操作时间
        ///</summary>
        [SugarColumn(ColumnName = "OpTime", ColumnDescription = "操作时间", IsNullable = true)]
        public DateTime OpTime { get; set; }

        /// <summary>
        /// 操作结束时间
        ///</summary>
        [SugarColumn(ColumnName = "OpFinishTime", ColumnDescription = "操作结束时间", IsNullable = true)]
        public DateTime OpFinishTime { get; set; }

        /// <summary>
        /// 操作人ID
        ///</summary>
        [SugarColumn(ColumnName = "OpUserId", ColumnDescription = "操作人ID", Length = 200, IsNullable = true)]
        public string OpUserId { get; set; }

        /// <summary>
        /// 操作人姓名
        ///</summary>
        [SugarColumn(ColumnName = "OpUser", ColumnDescription = "操作人姓名", Length = 200, IsNullable = true)]
        public string OpUser { get; set; }

        /// <summary>
        /// 操作人账号
        ///</summary>
        [SugarColumn(ColumnName = "OpAccount", ColumnDescription = "操作人账号", Length = 200, IsNullable = true)]
        public string OpAccount { get; set; }

        /// <summary>
        /// 耗时
        ///</summary>
        [SugarColumn(ColumnName = "TimeSpent", ColumnDescription = "耗时")]
        public double TimeSpent { get; set; }

        /// <summary>
        /// 耗时
        ///</summary>
        [SugarColumn(ColumnName = "TimeSpentStr", ColumnDescription = "耗时", Length = 200)]
        public string TimeSpentStr { get; set; }

        /// <summary>
        /// 异常具体消息
        ///</summary>
        [SugarColumn(ColumnName = "ExeMessage", ColumnDescription = "异常具体消息", ColumnDataType = StaticConfig.CodeFirst_BigString, IsNullable = true)]
        public string ExeMessage { get; set; }

        /// <summary>
        /// 类名称
        ///</summary>
        [SugarColumn(ColumnName = "ClassName", ColumnDescription = "类名称", Length = 200, IsNullable = true)]
        public string ClassName { get; set; }

        /// <summary>
        /// 方法名称
        ///</summary>
        [SugarColumn(ColumnName = "MethodName", ColumnDescription = "方法名称", Length = 200, IsNullable = true)]
        public string MethodName { get; set; }

        /// <summary>
        /// 请求方式
        ///</summary>
        [SugarColumn(ColumnName = "ReqMethod", ColumnDescription = "请求方式", Length = 200, IsNullable = true)]
        public string ReqMethod { get; set; }

        /// <summary>
        /// 请求头
        ///</summary>
        [SugarColumn(ColumnName = "ReqHeaders", ColumnDescription = "请求头", ColumnDataType = StaticConfig.CodeFirst_BigString, IsNullable = true)]
        public string ReqHeaders { get; set; }

        /// <summary>
        /// 返回头
        ///</summary>
        [SugarColumn(ColumnName = "ResultHeaders", ColumnDescription = "返回头", ColumnDataType = StaticConfig.CodeFirst_BigString, IsNullable = true)]
        public string ResultHeaders { get; set; }

        /// <summary>
        /// 请求地址
        ///</summary>
        [SugarColumn(ColumnName = "ReqUrl", ColumnDescription = "请求地址", ColumnDataType = StaticConfig.CodeFirst_BigString, IsNullable = true)]
        public string ReqUrl { get; set; }

        /// <summary>
        /// 请求参数
        ///</summary>
        [SugarColumn(ColumnName = "ParamJson", ColumnDescription = "请求参数", ColumnDataType = StaticConfig.CodeFirst_BigString, IsNullable = true)]
        public string ParamJson { get; set; }

        /// <summary>
        /// 返回结果
        ///</summary>
        [SugarColumn(ColumnName = "ResultJson", ColumnDescription = "返回结果", ColumnDataType = StaticConfig.CodeFirst_BigString, IsNullable = true)]
        public string ResultJson { get; set; }

        [SugarColumn(IsIgnore = true)]
        public Exception Exception { get; set; }
    }
}