﻿namespace SimpleX.Middlewares
{
    public class ResponseModel
    {
        public string ResponseBody { get; set; }
        public int ResponseStatus { get; set; }
        public string ResponseHeaders { get; set; }
        public DateTime FinishTime { get; set; }
        public Exception Exception { get; set; }
        public bool IsFile { get; set; }
    }
}