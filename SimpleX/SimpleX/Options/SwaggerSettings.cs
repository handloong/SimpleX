﻿namespace SimpleX
{
    public class SwaggerSettings
    {
        public bool Enabled { get; set; }
        public bool ShowWhenDocNamesNotExist { get; set; }
        public List<Docname> DocNames { get; set; }
    }

    public class Docname
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}