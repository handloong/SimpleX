﻿namespace SimpleX
{
    public class MQTTConfig
    {
        public string Url { get; set; }
        public string Server { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}