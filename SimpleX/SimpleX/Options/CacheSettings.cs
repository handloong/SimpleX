﻿namespace SimpleX
{
    public class CacheSettings
    {
        public bool UseRedis { get; set; }
        public RedisSettings RedisSettings { get; set; }
    }

    /// <summary>
    /// Redis设置
    /// </summary>
    public class RedisSettings
    {
        /// <summary>
        /// 连接地址
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// 端口号
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 数据库
        /// </summary>
        public int Db { get; set; }

        /// <summary>
        /// 超时时间
        /// </summary>
        public int TimeOut { get; set; } = 99999;
    }
}