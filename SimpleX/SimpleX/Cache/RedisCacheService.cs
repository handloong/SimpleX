﻿namespace SimpleX.Cache;

/// <summary>
/// Redis缓存
/// </summary>
public partial class RedisCacheService : ICacheService
{
    private readonly NewLife.Caching.ICache _cache;

    public RedisCacheService(NewLife.Caching.ICache cache)
    {
        _cache = cache;
    }

    #region 普通操作

    /// <inheritdoc/>
    public T Get<T>(string key)
    {
        return _cache.Get<T>(key);
    }

    public T GetOrAdd<T>(string key, Func<string, T> callback, int expire = -1)
    {
        return _cache.GetOrAdd<T>(key, callback, expire);
    }

    /// <inheritdoc/>
    public int Remove(params string[] keys)
    {
        return _cache.Remove(keys);
    }

    /// <inheritdoc/>
    public bool Set<T>(string key, T value, int expire = -1)
    {
        return _cache.Set(key, value, expire);
    }

    /// <inheritdoc/>
    public bool Set<T>(string key, T value, TimeSpan expire)
    {
        return _cache.Set(key, value, expire);
    }

    /// <inheritdoc/>
    public bool SetExpire(string key, TimeSpan expire)
    {
        return _cache.SetExpire(key, expire);
    }

    /// <inheritdoc/>
    public TimeSpan GetExpire(string key)
    {
        return _cache.GetExpire(key);
    }

    /// <inheritdoc/>
    public bool ContainsKey(string key)
    {
        return _cache.ContainsKey(key);
    }

    /// <inheritdoc/>
    public void Clear()
    {
        _cache.Clear();
    }

    /// <inheritdoc/>
    public void DelByPattern(string pattern)
    {
        if (string.IsNullOrEmpty(pattern))
            return;
        //pattern = Regex.Replace(pattern, @"\{*.\}", "(.*)");
        //var keys = fullRedis.Search(new SearchModel { Pattern = pattern });
        var keys = _cache.Keys?.Where(k => k.StartsWith(pattern));
        //var keys = GetAllKeys().Where(k => k.StartsWith(pattern));
        if (keys != null && keys.Any())
            _cache.Remove(keys.ToArray());
    }

    #endregion 普通操作

    #region 集合操作

    /// <inheritdoc/>
    public IDictionary<string, T> GetAll<T>(IEnumerable<string> keys)
    {
        return _cache.GetAll<T>(keys);
    }

    /// <inheritdoc/>
    public void SetAll<T>(IDictionary<string, T> values, int expire = -1)
    {
        _cache.SetAll(values, expire);
    }

    /// <inheritdoc/>
    public IDictionary<string, T> GetDictionary<T>(string key)
    {
        return _cache.GetDictionary<T>(key);
    }

    /// <inheritdoc/>
    public NewLife.Caching.IProducerConsumer<T> GetQueue<T>(string key)
    {
        return _cache.GetQueue<T>(key);
    }

    /// <inheritdoc/>
    public NewLife.Caching.IProducerConsumer<T> GetStack<T>(string key)
    {
        return _cache.GetStack<T>(key);
    }

    /// <inheritdoc/>
    public ICollection<T> GetSet<T>(string key)
    {
        return _cache.GetSet<T>(key);
    }

    #endregion 集合操作

    #region Hash

    public NewLife.Caching.RedisHash<string, T> GetHashMap<T>(string key)
    {
        return _cache.GetDictionary<T>(key) as NewLife.Caching.RedisHash<string, T>;
    }

    public void HashAdd<T>(string key, string hashKey, T value)
    {
        var hash = GetHashMap<T>(key);
        hash.Add(hashKey, value);
    }

    /// <inheritdoc/>
    public bool HashSet<T>(string key, Dictionary<string, T> dic)
    {
        var hash = GetHashMap<T>(key);
        return hash.HMSet(dic);
    }

    /// <inheritdoc/>
    public int HashDel<T>(string key, params string[] fields)
    {
        var hash = GetHashMap<T>(key);
        return hash.HDel(fields);
    }

    /// <inheritdoc/>
    public List<T> HashGet<T>(string key, params string[] fields)
    {
        var hash = GetHashMap<T>(key);
        var result = hash.HMGet(fields);
        return result.ToList();
    }

    /// <inheritdoc/>
    public T HashGetOne<T>(string key, string field)
    {
        var hash = GetHashMap<T>(key);
        var result = hash.HMGet(new string[] { field });
        return result[0];
    }

    /// <inheritdoc/>
    public IDictionary<string, T> HashGetAll<T>(string key)
    {
        var hash = GetHashMap<T>(key);
        return hash.GetAll();
    }

    #endregion

    #region 高级操作

    /// <inheritdoc/>
    public bool Add<T>(string key, T value, int expire = -1)
    {
        return _cache.Add(key, value, expire);
    }

    /// <inheritdoc/>
    public IList<T> GetList<T>(string key)
    {
        return _cache.GetList<T>(key);
    }

    /// <inheritdoc/>
    public T Replace<T>(string key, T value)
    {
        return _cache.Replace(key, value);
    }

    /// <inheritdoc/>
    public bool TryGetValue<T>(string key, out T value)
    {
        return _cache.TryGetValue(key, out value);
    }

    /// <inheritdoc/>
    public long Decrement(string key, long value)
    {
        return _cache.Decrement(key, value);
    }

    /// <inheritdoc/>
    public double Decrement(string key, double value)
    {
        return _cache.Decrement(key, value);
    }

    /// <inheritdoc/>
    public long Increment(string key, long value)
    {
        return _cache.Increment(key, value);
    }

    /// <inheritdoc/>
    public double Increment(string key, double value)
    {
        return _cache.Increment(key, value);
    }

    #endregion 高级操作

    #region 事务

    /// <inheritdoc/>
    public int Commit()
    {
        return _cache.Commit();
    }

    /// <inheritdoc/>
    public IDisposable AcquireLock(string key, int msTimeout)
    {
        return _cache.AcquireLock(key, msTimeout);
    }

    /// <inheritdoc/>
    public IDisposable AcquireLock(string key, int msTimeout, int msExpire, bool throwOnFailure)
    {
        return _cache.AcquireLock(key, msTimeout, msExpire, throwOnFailure);
    }

    #endregion 事务
}