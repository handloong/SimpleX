﻿using Masuit.Tools;

namespace SimpleX.Cache;

/// <summary>
/// 内存缓存
/// </summary>
public partial class MemoryCacheService : ICacheService
{
    public readonly NewLife.Caching.MemoryCache _memoryCache;

    public MemoryCacheService()
    {
        _memoryCache = new NewLife.Caching.MemoryCache();
    }

    #region 普通操作

    /// <inheritdoc/>
    public T Get<T>(string key)
    {
        return _memoryCache.Get<T>(key);
    }

    public T GetOrAdd<T>(string key, Func<string, T> callback, int expire = -1)
    {
        return _memoryCache.GetOrAdd<T>(key, callback, expire);
    }

    /// <inheritdoc/>
    public int Remove(params string[] keys)
    {
        return _memoryCache.Remove(keys);
    }

    /// <inheritdoc/>
    public bool Set<T>(string key, T value, int expire = -1)
    {
        return _memoryCache.Set(key, value.ToJson(), expire);
    }

    /// <inheritdoc/>
    public bool Set<T>(string key, T value, TimeSpan expire)
    {
        return _memoryCache.Set(key, value.ToJson(), expire);
    }

    /// <inheritdoc/>
    public bool SetExpire(string key, TimeSpan expire)
    {
        return _memoryCache.SetExpire(key, expire);
    }

    /// <inheritdoc/>
    public TimeSpan GetExpire(string key)
    {
        return _memoryCache.GetExpire(key);
    }

    /// <inheritdoc/>
    public bool ContainsKey(string key)
    {
        return _memoryCache.ContainsKey(key);
    }

    /// <inheritdoc/>
    public void Clear()
    {
        _memoryCache.Clear();
    }

    /// <inheritdoc/>
    public void DelByPattern(string pattern)
    {
        var keys = _memoryCache.Keys.ToList();//获取所有key
        keys.ForEach(it =>
        {
            if (it.Contains(pattern))//如果匹配
                _memoryCache.Remove(pattern);
        });
    }

    #endregion 普通操作

    #region 集合操作

    /// <inheritdoc/>
    public IDictionary<string, T> GetAll<T>(IEnumerable<string> keys)
    {
        IDictionary<string, T> result = default;//定义集合
        IDictionary<string, string> data = _memoryCache.GetAll<string>(keys);//获取数据
        data.ForEach(it =>
        {
            result.Add(it.Key, it.Value.ToObject<T>());//遍历数据,格式化并加到新的数据集合
        });
        return result;
    }

    /// <inheritdoc/>
    public void SetAll<T>(IDictionary<string, T> values, int expire = -1)
    {
        IDictionary<string, string> result = default;//定义集合
        values.ForEach(it =>
        {
            result.Add(it.Key, it.Value.ToJson());//遍历数据,格式化并加到新的数据集合
        });
        _memoryCache.SetAll(values, expire);
    }

    /// <inheritdoc/>
    public IDictionary<string, T> GetDictionary<T>(string key)
    {
        IDictionary<string, T> result = default;//定义集合
        var data = _memoryCache.GetDictionary<string>(key);
        data.ForEach(it =>
        {
            result.Add(it.Key, it.Value.ToObject<T>());//遍历数据,格式化并加到新的数据集合
        });
        return result;
    }

    /// <inheritdoc/>
    public NewLife.Caching.IProducerConsumer<T> GetQueue<T>(string key)
    {
        return _memoryCache.GetQueue<T>(key);
    }

    /// <inheritdoc/>
    public NewLife.Caching.IProducerConsumer<T> GetStack<T>(string key)
    {
        return _memoryCache.GetStack<T>(key);
    }

    /// <inheritdoc/>
    public ICollection<T> GetSet<T>(string key)
    {
        return _memoryCache.GetSet<T>(key);
    }

    #endregion 集合操作

    #region Hash

    /// <inheritdoc/>
    public void HashAdd<T>(string key, string hashKey, T value)
    {
        //获取字典
        var exist = _memoryCache.GetDictionary<T>(key);
        if (exist.ContainsKey(hashKey))//如果包含Key
            exist[hashKey] = value;//重新赋值
        else exist.Add(hashKey, value);//加上新的值
        _memoryCache.Set(key, exist);
    }

    //private IDictionary<string,T> GetDictionary(string key,string)

    /// <inheritdoc/>
    public bool HashSet<T>(string key, Dictionary<string, T> dic)
    {
        //获取字典
        var exist = _memoryCache.GetDictionary<T>(key);
        dic.ForEach(it =>
        {
            if (exist.ContainsKey(it.Key))//如果包含Key
                exist[it.Key] = it.Value;//重新赋值
            else exist.Add(it.Key, it.Value);//加上新的值
        });

        return true;
    }

    /// <inheritdoc/>
    public int HashDel<T>(string key, params string[] fields)
    {
        int result = 0;
        //获取字典
        var exist = _memoryCache.GetDictionary<T>(key);
        foreach (var field in fields)
        {
            if (field != null && exist.ContainsKey(field))//如果包含Key
            {
                exist.Remove(field);//删除
                result++;
            }
        }
        return result;
    }

    /// <inheritdoc/>
    public List<T> HashGet<T>(string key, params string[] fields)
    {
        List<T> list = new List<T>();
        //获取字典
        var exist = _memoryCache.GetDictionary<T>(key);
        foreach (var field in fields)
        {
            if (exist.ContainsKey(field))//如果包含Key
            {
                list.Add(exist[field]);
            }
            else { list.Add(default); }
        }
        return list;
    }

    /// <inheritdoc/>
    public T HashGetOne<T>(string key, string field)
    {
        //获取字典
        var exist = _memoryCache.GetDictionary<T>(key);

        exist.TryGetValue(field, out T result);
        var data = result.DeepClone();
        return data;
    }

    /// <inheritdoc/>
    public IDictionary<string, T> HashGetAll<T>(string key)
    {
        var data = _memoryCache.GetDictionary<T>(key);
        return data;
    }

    #endregion

    #region 高级操作

    /// <inheritdoc/>
    public bool Add<T>(string key, T value, int expire = -1)
    {
        return _memoryCache.Add(key, value.ToJson(), expire);
    }

    /// <inheritdoc/>
    public IList<T> GetList<T>(string key)
    {
        IList<T> result = default;//定义集合
        var data = _memoryCache.GetList<string>(key);
        data.ForEach(it =>
        {
            result.Add(it.ToObject<T>());//遍历数据,格式化并加到新的数据集合
        });
        return result;
    }

    /// <inheritdoc/>
    public T Replace<T>(string key, T value)
    {
        return _memoryCache.Replace(key, value);
    }

    /// <inheritdoc/>
    public bool TryGetValue<T>(string key, out T value)
    {
        var result = string.Empty;
        _ = _memoryCache.TryGetValue<string>(key, out result);
        value = result.ToObject<T>();
        return value == null;
    }

    /// <inheritdoc/>
    public long Decrement(string key, long value)
    {
        return _memoryCache.Decrement(key, value);
    }

    /// <inheritdoc/>
    public double Decrement(string key, double value)
    {
        return _memoryCache.Decrement(key, value);
    }

    /// <inheritdoc/>
    public long Increment(string key, long value)
    {
        return _memoryCache.Increment(key, value);
    }

    /// <inheritdoc/>
    public double Increment(string key, double value)
    {
        return _memoryCache.Increment(key, value);
    }

    #endregion 高级操作

    #region 事务

    /// <inheritdoc/>
    public int Commit()
    {
        return _memoryCache.Commit();
    }

    /// <inheritdoc/>
    public IDisposable AcquireLock(string key, int msTimeout)
    {
        return _memoryCache.AcquireLock(key, msTimeout);
    }

    /// <inheritdoc/>
    public IDisposable AcquireLock(string key, int msTimeout, int msExpire, bool throwOnFailure)
    {
        return _memoryCache.AcquireLock(key, msTimeout, msExpire, throwOnFailure);
    }

    #endregion 事务
}