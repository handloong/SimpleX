﻿using System.IO;

namespace SimpleX.Utils
{
    public class FileUtils
    {
        public static string ToAbsolute(string relative)
        {
            var dir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "WebFile");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            return Path.Combine(dir, relative);
        }
    }
}