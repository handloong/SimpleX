﻿using System.IO;

namespace SimpleX
{
    public class SeedDataUtil
    {
        public static List<T> GetSeedData<T>(string db, string jsonName)
        {
            var json = Path.Combine(AppContext.BaseDirectory, "SeedData", db, jsonName);//获取文件路径
            if (!File.Exists(json))
            {
                return [];
            }
            var jsonStr = File.ReadAllText(json, Encoding.UTF8);//读取json文件
            return Newtonsoft.Json.JsonConvert.DeserializeObject<List<T>>(jsonStr);
        }

        public static void WriteSeedDataJson(string db, DataTable data, string jsonName)
        {
            string rootDirectory = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "..", "..", ".."));

            var json = Path.Combine(rootDirectory, "SeedData", db);//获取文件路径
            if (!Directory.Exists(json))
            {
                Directory.CreateDirectory(json);
            }
            var jsonStr = data.ToJson(false, true);
            File.WriteAllText(Path.Combine(json, jsonName), jsonStr, Encoding.UTF8);//写入json文件
        }
    }
}