﻿using IPTools.Core;

namespace SimpleX
{
    public class IPHelper
    {
        public static string GetAddress(string ip)
        {
            var LoginAddress = "未知";
            try
            {
                var ipInfo = IpTool.Search(ip);//解析IP信息
                var LoginAddressList = new List<string>()
                {
                    ipInfo.Country,
                    ipInfo.Province,
                    ipInfo.City,
                    ipInfo.NetworkOperator
                };//定义登录地址列表
                LoginAddress = string.Join("|", LoginAddressList.Where(it => it != "0").ToList());//过滤掉0的信息并用|连接成字符串
            }
            catch
            {
            }
            return LoginAddress;
        }
    }
}