﻿namespace SimpleX.Application;

/// <summary>
/// 学生信息控制器
/// </summary>
[ApiExplorerSettings(GroupName = "Application")]
[Route("/app/student")]
public class StudentController : BaseControllerRoleAuthorize
{
    private readonly IStudentService _studentService;

    public StudentController(IStudentService studentService)
    {
        _studentService = studentService;
    }

    #region Get请求

    /// <summary>
    /// 通过分布式锁来扣除学生分
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("score")]
    [ActionPermission(ActionType.Query, "通过分布式锁来扣除学生分")]
    public async Task<dynamic> Page([FromQuery] BaseIdInput input)
    {
        return await _studentService.Score(input);
    }

    /// <summary>
    /// 学生信息分页查询
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("page")]
    [ActionPermission(ActionType.Query, "学生信息分页查询")]
    public async Task<dynamic> Page([FromQuery] StudentPageInput input)
    {
        return await _studentService.Page(input);
    }

    /// <summary>
    /// 学生信息详情
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("detail")]
    [ActionPermission(ActionType.Query, "学生信息详情")]
    public async Task<dynamic> Detail([FromQuery] BaseIdInput input)
    {
        return await _studentService.Detail(input);
    }

    #endregion

    #region Post请求

    /// <summary>
    /// 添加学生信息
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("add")]
    [ActionPermission(ActionType.Button, "添加学生信息")]
    public async Task Add([FromBody] StudentAddInput input)
    {
        await _studentService.Add(input);
    }

    /// <summary>
    /// 修改学生信息
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("edit")]
    [ActionPermission(ActionType.Button, "修改学生信息")]
    public async Task Edit([FromBody] StudentEditInput input)
    {
        await _studentService.Edit(input);
    }

    /// <summary>
    /// 删除学生信息
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("delete")]
    [ActionPermission(ActionType.Button, "删除学生信息")]
    public async Task Delete([FromBody] List<BaseIdInput> input)
    {
        await _studentService.Delete(input);
    }

    #endregion
}