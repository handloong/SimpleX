﻿namespace SimpleX.Application
{
    /// <summary>
    /// 学生表
    ///</summary>
    [SugarTable("app_student", TableDescription = "学生表")]
    [Tenant(SqlsugarConst.DB_Default)]
    [CodeGen]
    public class Student : BaseEntity
    {
        [SugarColumn(ColumnName = "Name", ColumnDescription = "姓名", Length = 30)]
        public string Name { get; set; }

        [SugarColumn(ColumnName = "Age", ColumnDescription = "年龄")]
        public int Age { get; set; }

        [SugarColumn(ColumnName = "Score", ColumnDescription = "表现分")]
        public int Score { get; set; }
    }
}