﻿namespace SimpleX.Application
{
    public class StudentSeedData : ISqlSugarEntitySeedData<Student>
    {
        //[IgnoreSeedDataUpdate]
        public IEnumerable<Student> SeedData()
        {
            return new List<Student>()
            {
                new Student{
                         Id = "STU001",
                         Name ="张三",
                         Age =14,
                         Score =20,
                         CreateDate = DateTime.Now,
                         CreateUserId ="Sys",
                         CreateUserName="Sys"
                },
                 new Student{
                         Id = "STU002",
                         Name ="李四",
                         Age =14,
                         Score =5,
                         CreateDate = DateTime.Now,
                         CreateUserId ="Sys",
                         CreateUserName="Sys"
                }
            };
        }
    }
}