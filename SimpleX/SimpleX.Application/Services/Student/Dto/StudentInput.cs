﻿namespace SimpleX.Application;

/// <summary>
/// 学生信息分页查询参数
/// </summary>
public class StudentPageInput : BasePageInput
{
}

/// <summary>
/// 添加学生信息参数
/// </summary>
public class StudentAddInput
{
    /// <summary>
    /// Name
    /// </summary>
    public long? Name { get; set; }

    /// <summary>
    /// Age
    /// </summary>
    public string Age { get; set; }
}

/// <summary>
/// 修改学生信息参数
/// </summary>
public class StudentEditInput : StudentAddInput
{
    /// <summary>
    /// Id
    /// </summary>
    [Required(ErrorMessage = "Id不能为空")]
    public string Id { get; set; }
}