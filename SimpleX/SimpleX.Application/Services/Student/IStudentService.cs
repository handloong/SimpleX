﻿namespace SimpleX.Application;

/// <summary>
/// 学生信息服务
/// </summary>
public interface IStudentService : ITransient
{
    /// <summary>
    /// 学生信息分页查询
    /// </summary>
    /// <param name="input">查询参数</param>
    /// <returns>分页结果</returns>
    Task<SqlSugarPagedList<Student>> Page(StudentPageInput input);

    /// <summary>
    /// 添加学生信息
    /// </summary>
    /// <param name="input">添加参数</param>
    /// <returns></returns>
    Task Add(StudentAddInput input);

    /// <summary>
    /// 删除学生信息
    /// </summary>
    /// <param name="input">删除参数</param>
    /// <returns></returns>
    Task Delete(List<BaseIdInput> input);

    /// <summary>
    /// 修改学生信息
    /// </summary>
    /// <param name="input">编辑参数</param>
    /// <returns></returns>
    Task Edit(StudentEditInput input);

    /// <summary>
    /// 学生信息详情
    /// </summary>
    /// <param name="input">Id参数</param>
    /// <returns>详细信息</returns>
    Task<Student> Detail(BaseIdInput input);

    /// <summary>
    /// 扣除学生分
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<dynamic> Score(BaseIdInput input);
}