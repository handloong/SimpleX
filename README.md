## 🎓 序言

生活不止眼前的代码，还有诗和远方！


# SimpleX 

####  :tw-1f331: 介绍
手搓通用脚手架SimpleX. 

SimpleX 取名参考 [SimpleAdmin ](https://gitee.com/zxzyjs/SimpleAdmin)(练习时长两年半的国东兄) 

RBAC的大部分代码都直接copy SimpleAdmin, 写代码就一个字:复制!粘贴!干!


####  :tw-1f34a: 后台开发&运行环境 [已经可以使用]

IDE: [VS 2022 17.8.1+](https://visualstudio.microsoft.com/zh-hans/downloads/)

环境: [.Net 8](https://dotnet.microsoft.com/en-us/download/dotnet/8.0)

后台文档: [F5启动,看Swagger,Debug]


####  :tw-1f350: 第三方类库 [Simplex MIT协议,使用时注意第三方类库协议]

1. Scrutor (MIT) :+1: 
2. Swashbuckle.AspNetCore (MIT):+1: 
3. [SqlSugarCore](https://www.donet5.com/Home/Doc) (MIT):+1: 
4. Mapster (MIT):+1: 
5. NLog (MIT):+1: 
6. NewLife.Redis (MIT):+1: 
7. Masuit.Tools.Core :+1:
8. FluentValidation.AspNetCore(MIT):+1: 
9. [DotNetCore.CAP(MIT):+1: ](https://cap.dotnetcore.xyz/user-guide/zh/getting-started/quick-start/)

####   :tw-2705:  准备开发的功能

- [x]  自动注入
- [x]  规范化结果添加请求耗时
- [x]  Jwt授权 
- [x]  接口权限
- [x]  全局错误拦截
- [x]  NLog日志
- [x]  SqlSugar [CodeFirst] [单例仓储] [无脑爽撸]
- [x]  缓存 [Redis/Memory]
- [x]  参数验证 
- [x]  Swagger
- [x]  事件总线
- [x]  代码生成
- [x]  通用导入导出
- [ ]  其他

####  :tw-274c: 不准备开发的功能

1. 动态API (虽然Service直接变成API但是不能解耦)
2. 配置中心 (按需引入第三方的)



####   :four_leaf_clover:  前台开发

UI框架 : 小诺 / Snowy 

本来想用其他的,对比了几个还是小诺的更赞

以下是小诺UI框架的说明: 

代码可用于个人项目等接私活或企业项目脚手架使用，Snowy全系开源版完全免费

二次开发如用于开源竞品请先联系群主沟通，禁止任何变相的二开行为，未经审核视为侵权

####  :tw-2705: 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
5.  欢迎大家PR  :+1: 

